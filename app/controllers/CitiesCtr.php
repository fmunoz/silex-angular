<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\CitiesLogic;


    class CitiesCtr extends Controller {


        public function __construct(CitiesLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $cities = $this->manager->getAll();
            return $this->json($cities);
        }

        function byProvince(Request $req) {
            $cities = $this->manager->byProvince($req->get("provinceId"));
            return $this->json($cities);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $city = $this->manager->get($id);
            return $this->json($city);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $city = $this->manager->create($data);
            return $this->json($city);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $city = $this->manager->destroy($id);
            return $this->json($city);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $city = $this->manager->update($id, $data);
            return $this->json($city);
        }

    }
}

?>
<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\ColaborationsLogic;


    class ColaborationsCtr extends Controller {


        public function __construct(ColaborationsLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $colaborations = $this->manager->getAll();
            return $this->json($colaborations);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $colaboration = $this->manager->get($id);
            return $this->json($colaboration);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $colaboration = $this->manager->create($data);
            return $this->json($colaboration);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $colaboration = $this->manager->destroy($id);
            return $this->json($colaboration);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $colaboration = $this->manager->update($id, $data);
            return $this->json($colaboration);
        }

        function getTotal(Request $req) {
            $id = $req->get("id");
            $total = $this->manager->getTotal($id);
            return $this->json(array("total" => $total));
        }

    }
}

?>
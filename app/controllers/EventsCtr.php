<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\EventsLogic;


    class EventsCtr extends Controller {


        public function __construct(EventsLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $events = $this->manager->getAll();
            return $this->json($events);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $event = $this->manager->get($id);
            return $this->json($event);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $event = $this->manager->create($data);
            return $this->json($event);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $event = $this->manager->destroy($id);
            return $this->json($event);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $event = $this->manager->update($id, $data);
            return $this->json($event);
        }

        function byArtist(Request $req) {
            $agents = $this->manager->byArtist();
            return $this->json($agents);
        }

        function getEventsPublic(Request $req) {
            $events = $this->manager->getEventsPublic();
            return $this->json($events);
        }
    }
}

?>
<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\MessagesLogic;


    class MessagesCtr extends Controller {


        public function __construct(MessagesLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $messages = $this->manager->getAll();
            return $this->json($messages);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $message = $this->manager->get($id);
            return $this->json($message);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $message = $this->manager->create($data);
            return $this->json($message);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $message = $this->manager->destroy($id);
            return $this->json($message);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $message = $this->manager->update($id, $data);
            return $this->json($message);
        }

        function getMessagesReceived(Request $req) {
            $messages = $this->manager->getMessagesReceived();
            return $this->json($messages);
        }

        function getMessagesSent(Request $req) {
            $messages = $this->manager->getMessagesSent();
            return $this->json($messages);
        }

    }
}

?>
<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\NewsLettersLogic;


    class NewsLettersCtr extends Controller {


        public function __construct(NewsLettersLogic $manager)
        {
            $this->manager = $manager;
        }
        
        function index(Request $req) {
            $newsLetters = $this->manager->getAll();
            return $this->json($newsLetters);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $newsLetters = $this->manager->get($id);
            return $this->json($newsLetters);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $newsLetters = $this->manager->create($data);
            return $this->json($newsLetters);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $newsLetters = $this->manager->destroy($id);
            return $this->json($newsLetters);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $newsLetters = $this->manager->update($id, $data);
            return $this->json($newsLetters);
        }

    }
}

?>
<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\PaymentsLogic;


    class PaymentsCtr extends Controller {


        public function __construct(PaymentsLogic $manager)
        {
            $this->manager = $manager;
        }
        
        function index(Request $req) {
            $payments = $this->manager->getAll();
            return $this->json($payments);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $payment = $this->manager->get($id);
            return $this->json($payment);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $payment = $this->manager->create($data);
            return $this->json($payment);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $payment = $this->manager->destroy($id);
            return $this->json($payment);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $payment = $this->manager->update($id, $data);
            return $this->json($payment);
        }

        function getDataAccess(Request $req) {
            $mercadopago = $this->manager->getDataAccess();
            return $this->json($mercadopago);
        }

        function createPreference(Request $req) {
            $data = $req->get("body");
            $preference = $this->manager->createPreference($data);
            return $this->json($preference);
        }

    }
}

?>
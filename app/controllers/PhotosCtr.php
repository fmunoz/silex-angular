<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\PhotosLogic;


    class PhotosCtr extends Controller {


        public function __construct(PhotosLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $photos = $this->manager->getAll();
            return $this->json($photos);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $photo = $this->manager->get($id);
            return $this->json($photo);
        }

        function create(Request $req) {
            $data = $req->get("body");
            $photo = $this->manager->create($data);
            return $this->json($photo);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $photo = $this->manager->destroy($id);
            return $this->json($photo);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $photo = $this->manager->update($id, $data);
            return $this->json($photo);
        }

    }
}

?>
<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\PromosLogic;


    class PromosCtr extends Controller {


        public function __construct(PromosLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $promos = $this->manager->getAll();
            return $this->json($promos);
        }

        function byEvent(Request $req) {
            $promos = $this->manager->byEvent($req->get("eventId"));
            return $this->json($promos);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $promo = $this->manager->get($id);
            return $this->json($promo);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $promo = $this->manager->create($data);
            return $this->json($promo);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $promo = $this->manager->destroy($id);
            return $this->json($promo);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $promo = $this->manager->update($id, $data);
            return $this->json($promo);
        }

    }
}

?>
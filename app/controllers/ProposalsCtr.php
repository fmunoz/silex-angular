<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\ProposalsLogic;


    class ProposalsCtr extends Controller {


        public function __construct(ProposalsLogic $manager)
        {
            $this->manager = $manager;
        }
        
        function index(Request $req) {
            $proposals = $this->manager->getAll();
            return $this->json($proposals);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $proposal = $this->manager->get($id);
            return $this->json($proposal);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $proposal = $this->manager->create($data);
            return $this->json($proposal);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $proposal = $this->manager->destroy($id);
            return $this->json($proposal);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $proposal = $this->manager->update($id, $data);
            return $this->json($proposal);
        }

        function byAgent(Request $req) {
            $agents = $this->manager->byAgent();
            return $this->json($agents);
        }

        function getProposalsPublics(Request $req) {
            $proposals = $this->manager->getProposalsPublics();
            return $this->json($proposals);
        }

        function myProposalsArtist(Request $req) {
            $proposals = $this->manager->myProposalsArtist();
            return $this->json($proposals);
        }

    }
}

?>
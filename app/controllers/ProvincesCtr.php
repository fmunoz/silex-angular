<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\ProvincesLogic;


    class ProvincesCtr extends Controller {


        public function __construct(ProvincesLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $provinces = $this->manager->getAll();
            return $this->json($provinces);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $province = $this->manager->get($id);
            return $this->json($province);
        }

        function create(Request $req) {
            $data = $req->get("body");
            $province = $this->manager->create($data);
            return $this->json($province);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $province = $this->manager->destroy($id);
            return $this->json($province);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $province = $this->manager->update($id, $data);
            return $this->json($province);
        }

    }
}

?>
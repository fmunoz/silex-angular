<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\ReleasesLogic;


    class ReleasesCtr extends Controller {


        public function __construct(ReleasesLogic $manager)
        {
            $this->manager = $manager;
        }
        
        function index(Request $req) {
            $releases = $this->manager->getAll();
            return $this->json($releases);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $release = $this->manager->get($id);
            return $this->json($release);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $release = $this->manager->create($data);
            return $this->json($release);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $release = $this->manager->destroy($id);
            return $this->json($release);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $release = $this->manager->update($id, $data);
            return $this->json($release);
        }

    }
}

?>
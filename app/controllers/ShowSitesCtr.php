<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\ShowSitesLogic;


    class ShowSitesCtr extends Controller {


        public function __construct(ShowSitesLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $showSites = $this->manager->getAll();
            return $this->json($showSites);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $showSite = $this->manager->get($id);
            return $this->json($showSite);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $showSite = $this->manager->create($data);
            return $this->json($showSite);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $showSite = $this->manager->destroy($id);
            return $this->json($showSite);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $showSite = $this->manager->update($id, $data);
            return $this->json($showSite);
        }

        function getShowSiteByUser(Request $req) {
            $showSite = $this->manager->getShowSiteByUser();
            return $this->json($showSite);
        }

        function getShowSitesPublics(Request $req) {
            $showSites = $this->manager->getShowSitesPublics();
            return $this->json($showSites);
        }

    }
}

?>
<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\TicketsLogic;


    class TicketsCtr extends Controller {


        public function __construct(TicketsLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $tickets = $this->manager->getAll();
            return $this->json($tickets);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $ticket = $this->manager->get($id);
            return $this->json($ticket);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $ticket = $this->manager->create($data);
            return $this->json($ticket);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $ticket = $this->manager->destroy($id);
            return $this->json($ticket);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $ticket = $this->manager->update($id, $data);
            return $this->json($ticket);
        }
        
        function getTicketFan(Request $req) {
            $tickets = $this->manager->getTicketFan();
            return $this->json($tickets);
        }
    }
}

?>
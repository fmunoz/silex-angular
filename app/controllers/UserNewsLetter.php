<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\UserNewsLettersLogic;


    class UserNewsLettersCtr extends Controller {


        public function __construct(UserNewsLettersLogic $manager)
        {
            $this->manager = $manager;
        }
        
        function index(Request $req) {
            $userNewsLetters = $this->manager->getAll();
            return $this->json($userNewsLetters);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $userNewsLetters = $this->manager->get($id);
            return $this->json($userNewsLetters);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $userNewsLetter = $this->manager->create($data);
            return $this->json($userNewsLetter);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $userNewsLetter = $this->manager->destroy($id);
            return $this->json($userNewsLetter);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $userNewsLetter = $this->manager->update($id, $data);
            return $this->json($userNewsLetter);
        }

    }
}

?>
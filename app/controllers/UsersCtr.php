<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use App\BusinessLogic\UsersLogic;


    class UsersCtr extends Controller {


        public function __construct(UsersLogic $manager)
        {
            $this->manager = $manager;
        }

        function loggedin(Request $req) {
            $logged = $this->manager->isLogged();
            return $this->json($logged);
        }

        function logout(Request $req) {
            $response = $this->manager->logout();
            return $this->json($response);
        }

        function login(Request $req) {
            $data = $req->get("body");
            $user = $this->manager->login($data);
            if($user) {
                return $this->json($user);
            }
            else {
                return $this->unauthorized();
            }
        }

        function create(Request $req) {
            $data = $req->get("body");
            $user = $this->manager->create($data);
            return $this->json($user);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $user = $this->manager->destroy($id);
            return $this->json($user);
        }

        function index(Request $req) {
            $users = $this->manager->getAll();
            return $this->json($users);
        }

        function getArtists(Request $req) {
            $artists = $this->manager->getArtists();
            return $this->json($artists);
        }

        function getPublicArtists(Request $req) {
            $artists = $this->manager->getPublicArtists();
            return $this->json($artists);
        }

        function getFans(Request $req) {
            $fans = $this->manager->getFans();
            return $this->json($fans);
        }

        function getShowSitesUsers(Request $req) {
            $showSitesUsers = $this->manager->getShowSitesUsers();
            return $this->json($showSitesUsers);
        }

        function getAgents(Request $req) {
            $agents = $this->manager->getAgents();
            return $this->json($agents);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $user = $this->manager->get($id);
            return $this->json($user);
        }

        function getLoginFacebookUrl(Request $req) {
            $url = $this->manager->getLoginFacebookUrl();
            return $this->json($url);
        }

        function getPreDataFacebook(Request $req) {
            $userFb = $this->manager->getDataFacebook();
            return $this->redirect("/#!/preLogin");
        }

        function getDataFacebook(Request $req) {
            $userFb = $this->manager->getDataFacebook();
            return $this->json($userFb);
        }

        function loginFan(Request $req) {
            $data = $req->get("body");
            $fan = $this->manager->loginFan($data);
            return $this->json($fan);
        }

        function registerFan(Request $req) {
            $data = $req->get("body");
            $fan = $this->manager->registerFan($data);
            return $this->json($fan);
        }

    }
}

?>

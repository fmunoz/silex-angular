<?php

namespace App\Controller {

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    use App\BusinessLogic\VotesLogic;


    class VotesCtr extends Controller {


        public function __construct(VotesLogic $manager)
        {
            $this->manager = $manager;
        }

        function index(Request $req) {
            $votes = $this->manager->getAll();
            return $this->json($votes);
        }

        function show(Request $req) {
            $id = $req->get("id");
            $vote = $this->manager->get($id);
            return $this->json($vote);
        }

        function create(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $vote = $this->manager->create($data);
            return $this->json($vote);
        }

        function destroy(Request $req) {
            $id = $req->get("id");
            $vote = $this->manager->destroy($id);
            return $this->json($vote);
        }

        function update(Request $req) {
            $id = $req->get("id");
            $data = $req->get("body");
            $vote = $this->manager->update($id, $data);
            return $this->json($vote);
        }

        function getVotesArtist(Request $req) {
            $votes = $this->manager->getVotesArtist();
            return $this->json($votes);
        }

        function getVotesOneArtist(Request $req) {
            $votes = $this->manager->getVotesOneArtist();
            return $this->json($votes);
        }
    }
}

?>
<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="admins")
 * @Entity(repositoryClass="App\DataAccessLayer\AdminRepository") 
 */
class Admin extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $email;
 
    /** @Column(type="text", nullable=true) */
    private $password;

    /** @Column(type="text", nullable=true) */
    private $dni;

    /** @Column(type="text", nullable=true) */
    private $name;

    /** @Column(type="text", nullable=true) */
    private $surname;

    /** @Column(type="text", nullable=true) */
    private $phone;

    /** @Column(type="text", nullable=true) */
    private $rol;


}

}
<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="cities")
 * @Entity(repositoryClass="App\DataAccessLayer\CityRepository") 
 */
class City extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $name;

    /** 
	 * @ManyToOne(targetEntity = "Province", inversedBy="cities" )
     * @JoinColumn(name = "province", referencedColumnName="id", onDelete="CASCADE" )
     */
  	private $province;

    /** @OneToMany(targetEntity="Proposal", mappedBy="city")*/
    private $proposals;

    /** @OneToMany(targetEntity="Event", mappedBy="city")*/
    private $events;

    /** @OneToMany(targetEntity="Vote", mappedBy="city")*/
    private $votes;

    /** @OneToMany(targetEntity="ShowSite", mappedBy="city")*/
    private $showSites;
}

}
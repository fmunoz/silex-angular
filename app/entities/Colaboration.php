<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="colaborations")
 * @Entity(repositoryClass="App\DataAccessLayer\ColaborationRepository") 
 */
class Colaboration extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    public function __construct(){
        $this->date = new \DateTime();
    }

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $mount;

    /** @Column(type="date") */
    private $date;

    /** 
     * @ManyToOne(targetEntity = "Proposal", inversedBy="colaborations" )
     * @JoinColumn(name = "proposal", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $proposal;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="colaborations" )
     * @JoinColumn(name = "user", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $user;
    
    
 
    
}

}
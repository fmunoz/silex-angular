<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="events")
 * @Entity(repositoryClass="App\DataAccessLayer\EventRepository") 
 */
class Event extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    public function __construct(){
       $this->dateInit = new \DateTime();
       $this->deadLine = new \DateTime();
    }

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="date") */
    private $dateInit;
 
    /** @Column(type="date", nullable=true) */
    private $deadLine;

    /** @Column(type="text") */
    private $colaborationsQuantity;

    /** @Column(type="text", nullable=true) */
    private $picture;

    /** @Column(type="text", nullable=true) */
    private $showSiteName;

    /** @Column(type="text", nullable=true) */
    private $showSiteAddres;

    /** @Column(type="text", nullable=true) */
    private $state;

    /** @Column(type="text", nullable=true) */
    private $ticketsAvailablesQuantity;

    /** @Column(type="text", nullable=true) */
    private $ticketsSoldsQuantity;

    /** @Column(type="text", nullable=true) */
    private $ticketPrice;

    /** @OneToMany(targetEntity="Ticket", mappedBy="event")*/
    private $tickets;

    /** @OneToMany(targetEntity="Promo", mappedBy="event")*/
    private $promo;

    /** 
     * @ManyToOne(targetEntity = "Province", inversedBy="events" )
     * @JoinColumn(name = "province", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $province;

    /** 
     * @ManyToOne(targetEntity = "City", inversedBy="events" )
     * @JoinColumn(name = "city", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $city;

    /** 
     * @ManyToOne(targetEntity = "ShowSite", inversedBy="events" )
     * @JoinColumn(name = "showSite", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $showSite;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="eventsAgent")
     * @JoinColumn(name = "agent", referencedColumnName="id", onDelete="CASCADE", nullable = true)
     */
    private $agent;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="eventsArtist")
     * @JoinColumn(name = "artist", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $artist;

}

}
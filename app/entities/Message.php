<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="messages")
 * @Entity(repositoryClass="App\DataAccessLayer\MessageRepository") 
 */
class Message extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    public function __construct(){
        $this->date = new \DateTime();
    }

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $subject;
 
    /** @Column(type="text") */
    private $content;

    /** @Column(type="text", nullable=true) */
    private $attachment;

    /** @Column(type="date") */
    private $date;

    /** @Column(type="text") */
    private $state;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="MessagesReceiver" )
     * @JoinColumn(name = "userReceiver", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $userReceiver;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="MessagesSender" )
     * @JoinColumn(name = "userSender", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $userSender;
}

}
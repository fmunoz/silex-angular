<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="newsLetters")
 * @Entity(repositoryClass="App\DataAccessLayer\NewsLetterRepository") 
 */
class NewsLetter extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    public function __construct(){
        $this->date = new \DateTime();
    }

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="date") */
    private $date;
 
    /** @Column(type="text") */
    private $subject;

    /** @Column(type="text", nullable=true) */
    private $content;

    
}

}
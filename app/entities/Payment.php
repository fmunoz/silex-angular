<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="payments")
 * @Entity(repositoryClass="App\DataAccessLayer\PaymentRepository") 
 */
class Payment extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $price;

    /** @Column(type="text") */
    private $concept;    

    /** @Column(type="text") */
    private $state;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="payments" )
     * @JoinColumn(name = "user", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $user;
    
}

}
<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="photos")
 * @Entity(repositoryClass="App\DataAccessLayer\PhotoRepository") 
 */
class Photo extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $path;

    /** 
	 * @ManyToOne(targetEntity = "User", inversedBy="photos" )
     * @JoinColumn(name = "user", referencedColumnName="id", onDelete="CASCADE" )
     */
  	private $user;
}

}
<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="promos")
 * @Entity(repositoryClass="App\DataAccessLayer\PromoRepository") 
 */
class Promo extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

     /** @Column(type="text", nullable=true) */
    private $discount;

     /** @Column(type="text", nullable=true) */
    private $description;

     /** @Column(type="text", nullable=true) */
    private $price;

    /** 
     * @ManyToOne(targetEntity = "Colaboration", inversedBy="Promo" )
     * @JoinColumn(name = "colaboration", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $colaboration;

    /** 
     * @ManyToOne(targetEntity = "Event", inversedBy="Promo" )
     * @JoinColumn(name = "event", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $event;
  
}

}
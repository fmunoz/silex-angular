<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="proposals")
 * @Entity(repositoryClass="App\DataAccessLayer\ProposalRepository") 
 */
class Proposal extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    public function __construct(){
        $this->dateInit = new \DateTime();
        $this->deadLine = new \DateTime();
    }

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="date") */
    private $dateInit;
 
    /** @Column(type="date", nullable=true) */
    private $deadLine;

    /** @Column(type="text") */
    private $colaborationsQuantity;

    /** @Column(type="text", nullable=true) */
    private $picture;

    /** @Column(type="text", nullable=true) */
    private $showSiteName;

    /** @Column(type="text", nullable=true) */
    private $showSiteAddres;

    /** @Column(type="text", nullable=true) */
    private $state;

    /** 
     * @ManyToOne(targetEntity = "Province", inversedBy="proposals" )
     * @JoinColumn(name = "province", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $province;

    /** 
     * @ManyToOne(targetEntity = "City", inversedBy="proposals" )
     * @JoinColumn(name = "city", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $city;

    /** 
     * @ManyToOne(targetEntity = "ShowSite", inversedBy="proposals" )
     * @JoinColumn(name = "showSite", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $showSite;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="proposalsAgent" )
     * @JoinColumn(name = "agent", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $agent;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="proposalsArtist" )
     * @JoinColumn(name = "artist", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $artist;

    /** OneToMany(targetEntity="Colaboration", mappedBy="proposal")*/
    private $colaborations;
    
    /** @OneToMany(targetEntity="Event", mappedBy="proposal")*/
    private $event;
}

}
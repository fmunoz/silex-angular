<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="provinces")
 * @Entity(repositoryClass="App\DataAccessLayer\ProvinceRepository") 
 */
class Province extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $name;

    /** @OneToMany(targetEntity="City", mappedBy="province")*/
  	private $cities;

    /** @OneToMany(targetEntity="Proposal", mappedBy="province")*/
    private $proposals;

    /** @OneToMany(targetEntity="Event", mappedBy="province")*/
    private $events;

    /** @OneToMany(targetEntity="Photo", mappedBy="province")*/
    private $votes;

    /** @OneToMany(targetEntity="ShowSite", mappedBy="province")*/
    private $showSites;
}

}
<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="releases")
 * @Entity(repositoryClass="App\DataAccessLayer\ReleaseRepository") 
 */
class Release extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    public function __construct(){
        $this->date = new \DateTime();
    }

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="date") */
    private $date;
 
    /** @Column(type="text") */
    private $title;

    /** @Column(type="text", nullable=true) */
    private $content;

    
}

}
<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="showSites")
 * @Entity(repositoryClass="App\DataAccessLayer\ShowSiteRepository") 
 */
class ShowSite extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $addres;

    /** @Column(type="text") */
    private $name;

    /** @Column(type="text") */
    private $capacity;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="ShowSite" )
     * @JoinColumn(name = "user", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $user;


    /** 
     * @ManyToOne(targetEntity = "Province", inversedBy="showSites" )
     * @JoinColumn(name = "province", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $province;

    /** 
     * @ManyToOne(targetEntity = "City", inversedBy="showSites" )
     * @JoinColumn(name = "city", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $city;

    /** @OneToMany(targetEntity="Proposal", mappedBy="showSite")*/
    private $proposals;

    /** @OneToMany(targetEntity="Event", mappedBy="showSite")*/
    private $events;



  
}

}
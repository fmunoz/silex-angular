<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="tickets")
 * @Entity(repositoryClass="App\DataAccessLayer\TicketRepository") 
 */
class Ticket extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $price;
 
    /** @Column(type="text", nullable=true) */
    private $description;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="tickets" )
     * @JoinColumn(name = "user", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $user;

    /** 
     * @Column(nullable=true)
     * @ManyToOne(targetEntity = "Event", inversedBy="tickets" )
     * @JoinColumn(name = "event", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $event;



}

}
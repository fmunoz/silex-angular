<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="users")
 * @Entity(repositoryClass="App\DataAccessLayer\UserRepository") 
 * @Table(name="users",uniqueConstraints={@UniqueConstraint(name="unique_users", columns={"email"})})
 */
class User extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="string", length=30) */
    private $email;
 
    /** @Column(type="text", nullable=true) */
    private $password;

    /** @Column(type="text", nullable=true) */
    private $idFacebook;

    /** @Column(type="text", nullable=true) */
    private $dni;

    /** @Column(type="text", nullable=true) */
    private $name;

    /** @Column(type="text", nullable=true) */
    private $surname;

    /** @Column(type="text", nullable=true) */
    private $phone;

    /** @Column(type="text", nullable=true) */
    private $artisticName;

    /** @Column(type="text", nullable=true) */
    private $profilePhoto;

    /** @Column(type="text", nullable=true) */
    private $rol;

    /** @Column(type="text", nullable=true) */
    private $state;

    /** @OneToMany(targetEntity="Colaboration", mappedBy="user")*/
    private $colaborations;

    /** @OneToMany(targetEntity="Ticket", mappedBy="user")*/
    private $tickets;

    /** @OneToMany(targetEntity="ShowSite", mappedBy="user")*/
    private $showSite;

    /** @OneToMany(targetEntity="Message", mappedBy="userReceiver")*/
    private $messagesReceiver;

    /** @OneToMany(targetEntity="Message", mappedBy="userSender")*/
    private $messagesSender;

    /** @OneToMany(targetEntity="Photo", mappedBy="user")*/
    private $photos;

    /** @OneToMany(targetEntity="Vote", mappedBy="user")*/
    private $votes;

    /** @OneToMany(targetEntity="Vote", mappedBy="artist")*/
    private $votesToFavor;

    /** @OneToMany(targetEntity="Proposal", mappedBy="agent")*/
    private $proposalsAgent;

    /** @OneToMany(targetEntity="Proposal", mappedBy="artist")*/
    private $proposalsArtist;

    /** @OneToMany(targetEntity="Event", mappedBy="agent")*/
    private $eventsAgent;

    /** @OneToMany(targetEntity="Event", mappedBy="artist")*/
    private $eventsArtist;
}

}
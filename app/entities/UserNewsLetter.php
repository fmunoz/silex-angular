<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="userNewsLetters")
 * @Entity(repositoryClass="App\DataAccessLayer\UserNewsLetterRepository") 
 */
class UserNewsLetter extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** @Column(type="text") */
    private $email;
   
    
}

}
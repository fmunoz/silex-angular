<?php

namespace App\Entity {


use \SerializableEntity;
use \ActiveEntity;

/**
 * @Table(name="votes")
 * @Entity(repositoryClass="App\DataAccessLayer\VoteRepository") 
 */
class Vote extends Entity {

    use ActiveEntity;
    use SerializableEntity;

    /** 
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /** 
	 * @ManyToOne(targetEntity = "Province", inversedBy="votes" )
     * @JoinColumn(name = "province", referencedColumnName="id", onDelete="CASCADE" )
     */
  	private $province;

    /** 
	 * @ManyToOne(targetEntity = "City", inversedBy="votes" )
     * @JoinColumn(name = "city", referencedColumnName="id", onDelete="CASCADE" )
     */
  	private $city;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="votes" )
     * @JoinColumn(name = "user", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $user;

    /** 
     * @ManyToOne(targetEntity = "User", inversedBy="votesToFavor" )
     * @JoinColumn(name = "artist", referencedColumnName="id", onDelete="CASCADE" )
     */
    private $artist;
  
}

}
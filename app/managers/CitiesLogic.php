<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class CitiesLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }
        
        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }
        
        public function byProvince($provinceId) {
            $all = $this->dataAccess->byProvince($provinceId);
            return $all;
        }

        public function get($id) {
            $city = $this->dataAccess->find($id);
            $city = ($city)? $city->toArray() : null; 
            return $city;
        }

        public function create($data) {
            try {
                $city = $this->entity->create($data);
                $city->persist();
                $this->em->flush();
                return $city->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "city" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $city = $this->dataAccess->find($id);
                $city->update($data);

                $this->em->flush();
                return $city->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "city" =>  $data);
            }
        }

        public function destroy($id) {
            $city = $this->dataAccess->find($id);
            if($city) {
                $city->remove();
                $this->em->flush();
                return $city->toArray();
            }
            else {
                return array("err" => "city not found");
            }
        }


    }

}
?>
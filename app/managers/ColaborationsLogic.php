<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;

    /**
    * 
    */
    class ColaborationsLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $colaboration = $this->dataAccess->find($id);
            $colaboration = ($colaboration)? $colaboration->toArray() : null; 
            return $colaboration;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                $data["date"] = "now";
                $data["user"] = $id;

                $colaboration = $this->entity->create($data);
                $colaboration->persist();
                $this->em->flush();
                return $colaboration->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "colaboration" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $colaboration = $this->dataAccess->find($id);
                $colaboration->update($data);

                $this->em->flush();
                return $colaboration->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "colaboration" =>  $data);
            }
        }

        public function destroy($id) {
            $colaboration = $this->dataAccess->find($id);
            if($colaboration) {
                $colaboration->remove();
                $this->em->flush();
                return $colaboration->toArray();
            }
            else {
                return array("err" => "colaboration not found");
            }
        }

        public function getTotal($id) {
            $total = $this->dataAccess->getTotal($id);
            return $total;   
        }

    }

}
?>
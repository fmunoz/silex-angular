<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;

    /**
    * 
    */
    class EventsLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $event = $this->dataAccess->find($id);
            $event = ($event)? $event->toArray() : null; 
            return $event;
        }

        public function create($data) {
            try {
                $event = $this->entity->create($data);
                $event->persist();
                $this->em->flush();
                return $event->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "event" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $event = $this->dataAccess->find($id);
                $event->update($data);

                $this->em->flush();
                return $event->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "event" =>  $data);
            }
        }

        public function destroy($id) {
            $event = $this->dataAccess->find($id);
            if($event) {
                $event->remove();
                $this->em->flush();
                return $event->toArray();
            }
            else {
                return array("err" => "event not found");
            }
        }

        public function byArtist() {
            $user = $this->session->get("user");
            $user = json_decode($user, true);            
            $id = $user["id"];
            $all = $this->dataAccess->byArtist($id);
            return $all;   
        }

        public function getEventsPublic() {
            $all = $this->dataAccess->getEventsPublic();
            return $all;
        }

    }

}
?>
<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;

    /**
    * 
    */
    class MessagesLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $message = $this->dataAccess->find($id);
            $message = ($message)? $message->toArray() : null; 
            return $message;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                $data["userSender"] = $id;

                $message = $this->entity->create($data);
                $message->persist();
                $this->em->flush();
                return $message->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "message" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $message = $this->dataAccess->find($id);
                $message->update($data);

                $this->em->flush();
                return $message->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "message" =>  $data);
            }
        }

        public function destroy($id) {
            $message = $this->dataAccess->find($id);
            if($message) {
                $message->remove();
                $this->em->flush();
                return $message->toArray();
            }
            else {
                return array("err" => "message not found");
            }
        }


        public function getMessagesReceived() {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                
                $messages = $this->dataAccess->getMessagesReceived($id);
                
                return $messages;   
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "messages received" =>  $data);
            }

        }


        public function getMessagesSent() {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                
                $messages = $this->dataAccess->getMessagesSent($id);
                
                return $messages;   
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "messages sent" =>  $data);
            }

        }

    }

}
?>
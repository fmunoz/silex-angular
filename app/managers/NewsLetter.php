<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class NewsLettersLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $newsLetter = $this->dataAccess->find($id);
            $newsLetter = ($newsLetter)? $newsLetter->toArray() : null; 
            return $newsLetter;
        }

        public function create($data) {
            try {
                $newsLetter = $this->entity->create($data);
                $newsLetter->persist();
                $this->em->flush();
                return $newsLetter->toArray();
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "newsLetter" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $newsLetter = $this->dataAccess->find($id);
                $newsLetter->update($data);

                $this->em->flush();
                return $newsLetter->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "newsLetter" =>  $data);
            }
        }

        public function destroy($id) {
            $newsLetter = $this->dataAccess->find($id);
            if($newsLetter) {
                $newsLetter->remove();
                $this->em->flush();
                return $newsLetter->toArray();
            }
            else {
                return array("err" => "newsLetter not found");
            }
        }

    }

}
?>
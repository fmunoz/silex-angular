<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class PaymentsLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session, $mp) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
            $this->mp = $mp;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $promo = $this->dataAccess->find($id);
            $promo = ($promo)? $promo->toArray() : null; 
            return $promo;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);
                $data["user"] = $user["id"];

                $payment = $this->entity->create($data);
                $payment->persist();
                
                $this->em->flush();
                
                return $payment->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "payment" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $payment = $this->dataAccess->find($id);
                $payment->update($data);

                $this->em->flush();
                return $payment->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "payment" =>  $data);
            }
        }

        public function destroy($id) {
            $payment = $this->dataAccess->find($id);
            if($payment) {
                $payment->remove();
                $this->em->flush();
                return $payment->toArray();
            }
            else {
                return array("err" => "payment not found");
            }
        }

        public function getDataAccess(){
            $accessToken = $this->mp->get_access_token();
            return $accessToken;   
        }

        public function createPreference($data){
            $preference_data = array(
                                    "items" => array(
                                       array(
                                           "title" => $data["title"],
                                           "quantity" => intval($data["quantity"]),
                                           "currency_id" => $data["currency_id"],
                                           "unit_price" => floatval($data["unit_price"])
                                       )
                                    )
                                );
            $preferenceResult = $this->mp->create_preference($preference_data);

            return $preferenceResult;
        }

        public function createPayment(){
            $user = $this->session->get("user");
            $user = json_decode($user, true);
            $data["user"] = $user["id"];

            $payment = $this->entity->create($data);
            $payment->persist();
                
            $this->em->flush();

            return $payment->toArray();     

        }

    }

}
?>
<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class PhotosLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $photo = $this->dataAccess->find($id);
            $photo = ($photo)? $photo->toArray() : null; 
            return $photo;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                $data["user"] = $id;
                
                $photo = $this->entity->create($data);
                $photo->persist();
                $this->em->flush();
                return $photo->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "photo" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $photo = $this->dataAccess->find($id);
                $photo->update($data);

                $this->em->flush();
                return $photo->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "photo" =>  $data);
            }
        }

        public function destroy($id) {
            $photo = $this->dataAccess->find($id);
            if($photo) {
                $photo->remove();
                $this->em->flush();
                return $photo->toArray();
            }
            else {
                return array("err" => "photo not found");
            }
        }

    }

}
?>
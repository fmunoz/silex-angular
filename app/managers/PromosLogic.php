<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class PromosLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function byEvent($eventId) {
            $all = $this->dataAccess->byEvent($eventId);
            return $all;
        }

        public function get($id) {
            $promo = $this->dataAccess->find($id);
            $promo = ($promo)? $promo->toArray() : null; 
            return $promo;
        }

        public function create($data) {
            try {
                $promo = $this->entity->create($data);
                $promo->persist();
                $this->em->flush();
                return $promo->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "promo" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $promo = $this->dataAccess->find($id);
                $promo->update($data);

                $this->em->flush();
                return $promo->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "promo" =>  $data);
            }
        }

        public function destroy($id) {
            $promo = $this->dataAccess->find($id);
            if($promo) {
                $promo->remove();
                $this->em->flush();
                return $promo->toArray();
            }
            else {
                return array("err" => "promo not found");
            }
        }

    }

}
?>
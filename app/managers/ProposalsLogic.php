<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class ProposalsLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $proposal = $this->dataAccess->find($id);
            $proposal = ($proposal)? $proposal->toArray() : null; 
            return $proposal;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);
                if($user["rol"] == "artist"){          
                    $id = $user["id"];
                    $data["artist"] = $id;
                }
                if($user["rol"] == "agent"){          
                    $id = $user["id"];
                    $data["agent"] = $id;
                }

                $proposal = $this->entity->create($data);
                $proposal->persist();
                $this->em->flush();
                return $proposal->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "proposal" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $proposal = $this->dataAccess->find($id);
                $proposal->update($data);

                $this->em->flush();
                return $proposal->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "proposal" =>  $data);
            }
        }

        public function destroy($id) {
            $proposal = $this->dataAccess->find($id);
            if($proposal) {
                $proposal->remove();
                $this->em->flush();
                return $proposal->toArray();
            }
            else {
                return array("err" => "proposal not found");
            }
        }

        public function byAgent() {
            $user = $this->session->get("user");
            $user = json_decode($user, true);            
            $id = $user["id"];
            $all = $this->dataAccess->byAgent($id);
            return $all;   
        }
        
        public function getProposalsPublics() {
            $all = $this->dataAccess->getProposalsPublics();
            return $all;
        }

        public function myProposalsArtist() {
            $user = $this->session->get("user");
            $user = json_decode($user, true);            
            $id = $user["id"];
            $all = $this->dataAccess->myProposalsArtist($id);
            return $all;   
        }

    }

}
?>
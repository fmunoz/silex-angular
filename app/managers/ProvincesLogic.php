<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class ProvincesLogic extends BusinessLogic{
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $province = $this->dataAccess->find($id);
            $province = ($province)? $province->toArray() : null; 
            return $province;
        }

        public function create($data) {
            try {
                $province = $this->entity->create($data);
                $province->persist();
                $this->em->flush();
                return $province->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "province" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $province = $this->dataAccess->find($id);
                $province->update($data);

                $this->em->flush();
                return $province->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "province" =>  $data);
            }
        }

        public function destroy($id) {
            $province = $this->dataAccess->find($id);
            if($province) {
                $province->remove();
                $this->em->flush();
                return $province->toArray();
            }
            else {
                return array("err" => "province not found");
            }
        }

    }

}
?>
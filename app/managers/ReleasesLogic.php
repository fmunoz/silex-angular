<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class ReleasesLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $release = $this->dataAccess->find($id);
            $release = ($release)? $release->toArray() : null; 
            return $release;
        }

        public function create($data) {
            try {
                $release = $this->entity->create($data);
                $release->persist();
                $this->em->flush();
                return $release->toArray();
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "release" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $release = $this->dataAccess->find($id);
                $release->update($data);

                $this->em->flush();
                return $release->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "release" =>  $data);
            }
        }

        public function destroy($id) {
            $release = $this->dataAccess->find($id);
            if($release) {
                $release->remove();
                $this->em->flush();
                return $release->toArray();
            }
            else {
                return array("err" => "release not found");
            }
        }

    }

}
?>
<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class ShowSitesLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $showSite = $this->dataAccess->find($id);
            $showSite = ($showSite)? $showSite->toArray() : null; 
            return $showSite;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                $data["user"] = $id;
                
                $showSite = $this->entity->create($data);
                $showSite->persist();
                $this->em->flush();
                return $showSite->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "showSite" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $showSite = $this->dataAccess->find($id);
                $showSite->update($data);

                $this->em->flush();
                return $showSite->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "showSite" =>  $data);
            }
        }

        public function destroy($id) {
            $showSite = $this->dataAccess->find($id);
            if($showSite) {
                $showSite->remove();
                $this->em->flush();
                return $showSite->toArray();
            }
            else {
                return array("err" => "showSite not found");
            }
        }
        
        public function getShowSiteByUser() {
            $user = $this->session->get("user");
            $user = json_decode($user, true);            
            $id = $user["id"];
            $showSite = $this->dataAccess->getShowSiteByUser($id);
            return $showSite;   
        }

        public function getShowSitesPublics() {
            $showSites = $this->dataAccess->getShowSitesPublics();
            return $showSites;
        }

    }

}
?>
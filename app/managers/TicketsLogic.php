<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class TicketsLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $ticket = $this->dataAccess->find($id);
            $ticket = ($ticket)? $ticket->toArray() : null; 
            return $ticket;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                $data["user"] = $id;
                
                $ticket = $this->entity->create($data);
                $ticket->persist();
                $this->em->flush();
                return $ticket->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "ticket" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $ticket = $this->dataAccess->find($id);
                $ticket->update($data);

                $this->em->flush();
                return $ticket->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "ticket" =>  $data);
            }
        }

        public function destroy($id) {
            $ticket = $this->dataAccess->find($id);
            if($ticket) {
                $ticket->remove();
                $this->em->flush();
                return $ticket->toArray();
            }
            else {
                return array("err" => "ticket not found");
            }
        }
        
        public function getTicketFan() {
            $user = $this->session->get("user");
            $user = json_decode($user, true);            
            $id = $user["id"];
            $all = $this->dataAccess->byUser($id);
            return $all;
        }

    }

}
?>
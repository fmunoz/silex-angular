<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class UserNewsLettersLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $userNewsLetter = $this->dataAccess->find($id);
            $userNewsLetter = ($userNewsLetter)? $userNewsLetter->toArray() : null; 
            return $userNewsLetter;
        }

        public function create($data) {
            try {
                $userNewsLetter = $this->entity->create($data);
                $userNewsLetter->persist();
                $this->em->flush();
                return $userNewsLetter->toArray();
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "userNewsLetter" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $userNewsLetter = $this->dataAccess->find($id);
                $userNewsLetter->update($data);

                $this->em->flush();
                return $userNewsLetter->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "userNewsLetter" =>  $data);
            }
        }

        public function destroy($id) {
            $userNewsLetter = $this->dataAccess->find($id);
            if($userNewsLetter) {
                $userNewsLetter->remove();
                $this->em->flush();
                return $userNewsLetter->toArray();
            }
            else {
                return array("err" => "userNewsLetter not found");
            }
        }

    }

}
?>
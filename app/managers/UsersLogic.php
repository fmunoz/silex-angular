<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    use App\Entity\Entity;

    /**
    * 
    */
    class UsersLogic extends BusinessLogic {
        
        protected $dataAccess;
        protected $em;
        protected $accessToken;

        public function __construct(EntityRepository $dataAccess, Entity $entity, EntityManager $em, Session $session, $facebook) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->facebook = $facebook;
            $this->em = $em;
        }

        public function isLogged() {
            return ($this->session->get("user")) ? $this->session->get("user") : 0;
        }

        public function login($data) {
            $user = $this->dataAccess->getByEmailAndPass($data["email"], $data["password"]);
            if($user) {
                $user["password"] = "";
                $this->session->set("user", json_encode($user));
            }
            return $user;
        }

        public function logout() {
            $this->session->invalidate();
            return json_encode(array("success" => 1));
        }

        public function create($data) {
            $data["state"] = ($data["rol"] == "fan") ? "Publico" : "Oculto";
            $user = $this->entity->create($data);
            $user->persist();
            $this->em->flush();
            return $user->toArray();
        }

        public function destroy($id) {
            $user = $this->dataAccess->find($id);
            if($user) {
                $user->remove();
                $this->em->flush();
                return $user->toArray();
            }
            else {
                return array("err" => "user not found");
            }
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }
        
        public function getArtists() {
            $all = $this->dataAccess->getArtists();
            return $all;   
        }

        public function getPublicArtists() {
            $all = $this->dataAccess->getPublicArtists();
            return $all;   
        }

        public function getFans() {
            $all = $this->dataAccess->getFans();
            return $all;   
        }

        public function getShowSitesUsers() {
            $all = $this->dataAccess->getShowSitesUsers();
            return $all;   
        }

        public function getAgents() {
            $all = $this->dataAccess->getAgents();
            return $all;   
        }

        public function get($id) {
            $user = $this->dataAccess->find($id);
            $user = ($user)? $user->toArray() : null; 
            return $user;
        }

        public function getLoginFacebookUrl(){
            $params = array(
              'redirect_uri' => 'http://localhost:8008/getPreDataFacebook'
            );
            //$this->accessToken = $this->facebook->getApplicationAccessToken();
            $loginUrl = $this->facebook->getLoginUrl($params);
            
            return array("loginUrl" => $loginUrl);
        }

        public function getDataFacebook(){
            //$this->facebook->setAccessToken($this->accessToken);
            $userFb = $this->facebook->api("/me", 'GET');
            return $userFb; //retornar datos de login, ya es array
        }

        public function loginFan($data) {
            //idFacebook
            print_r($data);
            $user = $this->dataAccess->getByIdFacebook($data["idFacebook"]);
            if($user) {
                $user["password"] = "";
                $this->session->set("user", json_encode($user));
            }
            return $user;
        }

        public function registerFan($data) {
            //idFacebook - name - surname 
            $user = $this->dataAccess->getByIdFacebook($data["idFacebook"]);
            if($user == null)
            {
                $data["rol"] = "fan";
                $data["state"] = "Publico";
                $user = $this->entity->create($data);
                $user->persist();
                $this->em->flush();
                $user = $user->toArray();
            }
            
            return $user;
        }
    

    }

}
?>

<?php

namespace App\BusinessLogic {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Exception;
    /**
    * 
    */
    class VotesLogic {
        
        protected $dataAccess;
        protected $em;

        public function __construct(EntityRepository $dataAccess, $entity, EntityManager $em, Session $session) {
            $this->dataAccess = $dataAccess;
            $this->entity = $entity;
            $this->session = $session;
            $this->em = $em;
        }

        public function getAll() {
            $all = $this->dataAccess->getAll();
            return $all;
        }

        public function get($id) {
            $vote = $this->dataAccess->find($id);
            $vote = ($vote)? $vote->toArray() : null; 
            return $vote;
        }

        public function create($data) {
            try {
                $user = $this->session->get("user");
                $user = json_decode($user, true);            
                $id = $user["id"];
                $data["user"] = $id;
                
                $vote = $this->entity->create($data);
                $vote->persist();
                $this->em->flush();
                return $vote->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "vote" =>  $data);
            }

        }

        public function update($id, $data) {
            try {
                $vote = $this->dataAccess->find($id);
                $vote->update($data);

                $this->em->flush();
                return $vote->toArray();             
            } 
            catch (Exception $e) {
                return array("err" => $e->getMessage(), "vote" =>  $data);
            }
        }

        public function destroy($id) {
            $vote = $this->dataAccess->find($id);
            if($vote) {
                $vote->remove();
                $this->em->flush();
                return $vote->toArray();
            }
            else {
                return array("err" => "vote not found");
            }
        }

        public function getVotesArtist() {
            $all = $this->dataAccess->getVotesArtist();
            return $all;   
        }

        public function getVotesOneArtist() {
            $user = $this->session->get("user");
            $user = json_decode($user, true);            
            $id = $user["id"];

            $all = $this->dataAccess->getVotesOneArtist($id);
            return $all;   
        }

    }

}
?>
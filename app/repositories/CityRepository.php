<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class CityRepository extends EntityRepository {

        use \GenericQueries;

    	public function byProvince($provinceId) {
    		$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.province = :provinceId");

            $query = $query->setParameter("provinceId", $provinceId);
            $query = $query->getQuery();
            return $query->getArrayResult();
        }

    }

}

?>
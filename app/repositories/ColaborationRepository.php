<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class ColaborationRepository extends EntityRepository {

    	use \GenericQueries;

    	public function getTotal($proposalId) {
    		$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select('sum(r.mount) as total')->from($this->getClassName(), $alias)->where("r.proposal = :proposalId");

            $query = $query->setParameter("proposalId", $proposalId);
            $query = $query->getQuery();
            return $query->getArrayResult()[0][total];
        }
        
    }

}

?>
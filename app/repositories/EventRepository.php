<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class EventRepository extends EntityRepository {

    	use \GenericQueries;

    	public function byArtist($userId) {
    		$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.artist = :userId");

            $query = $query->setParameter("userId", $userId);
            $query = $query->getQuery();
            return $query->getArrayResult();
        }

        public function getEventsPublic() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.state = 'Publico'");

            $query = $query->getQuery();

            $all = $query->getResult();
            $result = array();
            foreach($all as $item) {
                $result[] = $item->toArray();
            }
            return $result;
            
            
        }

    }

}

?>
<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class MessageRepository extends EntityRepository {

    	use \GenericQueries;

    	public function getMessagesReceived($idUser){
    		$criterio = array("userReceiver" => $idUser);
    		$all = $this->findBy($criterio, array('date' =>'DESC'));

    		$result = array();
        	foreach($all as $item) {
            	$result[] = $item->toArray();
        	}
    		return $result;
    	}


    	public function getMessagesSent($idUser){
    		$criterio = array("userSender" => $idUser);
    		$all = $this->findBy($criterio, array('date' =>'DESC'));

    		$result = array();
        	foreach($all as $item) {
            	$result[] = $item->toArray();
        	}
    		return $result;
    	}
        
        public function getByEmailAndPass($email, $pass) {
            $user = $this->findOneBy(array("email" => $email, "password" => $pass));
            return ($user != null) ? $user->toArray() : null;
        }
    }

}

?>
<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class PromoRepository extends EntityRepository {

    	use \GenericQueries;

    	public function byEvent($eventId) {
    		$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.event = :eventId");

            $query = $query->setParameter("eventId", $eventId);
            $query = $query->getQuery();
            return $query->getArrayResult();
        }
        
    }

}

?>
<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class ProposalRepository extends EntityRepository {

        use \GenericQueries;
		
		public function byAgent($agentId) {
    		$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.agent = :agentId");

            $query = $query->setParameter("agentId", $agentId);
            $query = $query->getQuery();
            
            $all = $query->getResult();
            $result = array();
            foreach($all as $item) {
                $result[] = $item->toArray();
            }
            return $result;
        }
            	
        public function getProposalsPublics() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.state = 'Publico'");

            $query = $query->getQuery();
            
            $all = $query->getResult();
            $result = array();
            foreach($all as $item) {
                $result[] = $item->toArray();
            }
            return $result;

        }

        
        public function myProposalsArtist($artistId) {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.artist = :artistId");

            $query = $query->setParameter("artistId", $artistId);

            $query = $query->getQuery();
            
            $all = $query->getResult();
            $result = array();
            foreach($all as $item) {
                $result[] = $item->toArray();
            }
            return $result;

        }
    }

}

?>
<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class ShowSiteRepository extends EntityRepository {

    	use \GenericQueries;

      public function getShowSiteByUser($user) {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.user = :user");

            $query = $query->setParameter("user", $user);

            $query = $query->getQuery();
            
            $all = $query->getResult();
            $result = array();
            foreach($all as $item) {
                $result[] = $item->toArray();
            }
            return $result[0];
        }

        public function getShowSitesPublics() {
            $alias = 'ss';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select("ss.id as showSiteId","ss.name as showSiteName", "ss.addres as showSiteAddres", "ss.capacity as showSiteCapacity",
             "p.id as provinceId", "p.name as provinceName",
              "c.id as cityId", "c.name as cityName",
               "u.id as userId", "u.name as userName", "u.surname as userSurname")->from($this->getClassName(), $alias)
               ->join("ss.user",'u')
               ->join("ss.province",'p')
               ->join("ss.city",'c')
               ->andWhere("p.id = ss.province")
               ->andWhere("c.id = ss.city")
               ->andWhere("u.id = ss.user")
               ->andWhere("u.state = 'Publico'");
               
            
            $query = $query->getQuery();
            
            return $query->getArrayResult();
        }

    }

}

?>
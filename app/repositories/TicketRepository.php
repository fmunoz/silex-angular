<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class TicketRepository extends EntityRepository {

    	use \GenericQueries;

    	public function byUser($userId) {
    		$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.user = :userId");

            $query = $query->setParameter("userId", $userId);
            $query = $query->getQuery();
            return $query->getArrayResult();
        }
        
    }

}

?>
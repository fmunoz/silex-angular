<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class UserRepository extends EntityRepository {

        use \GenericQueries;

        public function getByEmailAndPass($email, $pass) {
            $user = $this->findOneBy(array("email" => $email, "password" => $pass));
            return ($user != null) ? $user->toArray() : null;
        }

        public function getArtists() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.rol = 'artist'");

            $query = $query->getQuery();
            
            return $query->getArrayResult();
        }

        public function getPublicArtists() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->andWhere("r.rol = 'artist'")->andWhere("r.state = 'Publico'");

            $query = $query->getQuery();
            
            return $query->getArrayResult();
        }

        public function getFans() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.rol = 'fan'");

            $query = $query->getQuery();
            
            return $query->getArrayResult();
        }

        public function getShowSitesUsers() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.rol = 'showSite'");

            $query = $query->getQuery();
            
            return $query->getArrayResult();
        }

        public function getAgents() {
            $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select($alias)->from($this->getClassName(), $alias)->where("r.rol = 'agent'");

            $query = $query->getQuery();
            
            return $query->getArrayResult();
        }

        public function getByIdFacebook($idFacebook){
            $user = $this->findOneBy(array("idFacebook" => $idFacebook));
            return ($user != null) ? $user->toArray() : null;
        }

    }

}

?>
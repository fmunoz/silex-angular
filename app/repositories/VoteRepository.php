<?php

namespace App\DataAccessLayer {

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Mapping\ClassMetadata;

    class VoteRepository extends EntityRepository {

    	use \GenericQueries;

    	public function getVotesArtist() {
        	$alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select("count(r.artist) as amount","c.name as city","p.name as province","a.artisticName as artist")->from($this->getClassName(), $alias)
               ->join("r.province",'p')
               ->join("r.city",'c')
               ->join("r.artist",'a')
               ->andWhere("p.id = r.province")
               ->andWhere("c.id = r.city")
               ->andWhere("a.id = r.artist")
               ->groupBy("p.name,c.name,r.artist"); 
            
            $query = $query->getQuery();
            
            return $query->getArrayResult();
      }

      public function getVotesOneArtist($artistId) {
          $alias = 'r';
            $query = $this->getEntityManager()->createQueryBuilder($alias);
            $query = $query->select("count(r.artist) as amount","c.name as city","p.name as province","a.artisticName as artist")->from($this->getClassName(), $alias)
               ->join("r.province",'p')
               ->join("r.city",'c')
               ->join("r.artist",'a')
               ->andWhere("p.id = r.province")
               ->andWhere("c.id = r.city")
               ->andWhere(":artistId = r.artist")
               ->groupBy("p.name,c.name,r.artist"); 

            $query = $query->setParameter("artistId", $artistId);
            $query = $query->getQuery();
            
            return $query->getArrayResult();
      }
        
    }

}

?>
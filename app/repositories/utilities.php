<?php
/**
* Use it in a Repository
* class UserRepository {
* ...
*    use \GenericQueries;
* ...
* }
*/

trait GenericQueries {
    public function getAll() {
        $all = $this->findAll();
        $result = array();
        foreach($all as $item) {
            $result[] = $item->toArray();
        }
        return $result;
    }

}


?>
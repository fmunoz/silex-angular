<?php

$app->get("/loggedinAdmin", "admins.controller:loggedin")
        ->bind("loggedinAdmin");

$app->get("/logoutAdmin", "admins.controller:logout")
        ->bind("logoutAdmin");

$app->post("/loginAdmin", "admins.controller:login")
        ->bind("loginAdmin")
        ->before(App\Authorizations\Basic::getMustBeAnonymous($app));

$app->post("/registerAdmin", "admins.controller:create")
        ->bind("registeruserAdmin")
        ->before(App\Authorizations\Basic::getMustBeAnonymous($app));

$app->delete("/usersAdmin/{id}", "admins.controller:destroy")
        ->bind("destroyuserAdmin");
        //para testear se utilizó esta función y en la app real
        //tal vez haga falta que haya un usuario ADMIN loggeado
?>
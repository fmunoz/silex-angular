<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/cities", "cities.controller:create")
        ->bind("createcitie")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/cities", "cities.controller:index")
        ->bind("cities")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/cities/byprovince/{provinceId}", "cities.controller:byProvince")
        ->bind("cities")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/cities/{id}", "cities.controller:show")
        ->bind("showcitie")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/cities/{id}", "cities.controller:destroy")
        ->bind("destroycitie")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/cities/{id}", "cities.controller:update")
        ->bind("updatecitie")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/colaborations", "colaborations.controller:create")
        ->bind("createcolaboration")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/colaborations", "colaborations.controller:index")
        ->bind("colaborations")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/colaborations/{id}", "colaborations.controller:show")
        ->bind("showcolaboration")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/colaborations/total/{id}", "colaborations.controller:getTotal")
        ->bind("showcolaboration")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/colaborations/{id}", "colaborations.controller:destroy")
        ->bind("destroycolaboration")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/colaborations/{id}", "colaborations.controller:update")
        ->bind("updatecolaboration")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

<?php

$protectedRoutes = $app["controllers_factory"];

$app->post("/events", "events.controller:create")
        ->bind("createevent")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/events", "events.controller:index")
        ->bind("events")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->delete("/events/{id}", "events.controller:destroy")
        ->bind("destroyevent")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/events/{id}", "events.controller:update")
        ->bind("updateevent")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/eventsPublics","events.controller:getEventsPublic")
        ->bind("eventsPublics")
        ->before(App\Authorizations\Basic::getMustBeFan($app));

$app->get("/events/byartist", "events.controller:byArtist")
        ->bind("eventbyartist")
        ->before(App\Authorizations\Basic::getMustBeArtist($app));

$app->get("/events/{id}", "events.controller:show")
        ->bind("showevent")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

?>

<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/messages", "messages.controller:create")
        ->bind("createmessage")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/messages", "messages.controller:index")
        ->bind("messages")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/messages/{id}", "messages.controller:show")
        ->bind("showmessage")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/messages/{id}", "messages.controller:destroy")
        ->bind("destroymessage")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/messages/{id}", "messages.controller:update")
        ->bind("updatemessage")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/messagesReceived", "messages.controller:getMessagesReceived")
        ->bind("messages received")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/messagesSent", "messages.controller:getMessagesSent")
        ->bind("messages sent")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

?>

<?php

namespace App\Authorizations {

    class Basic {

        public static function getMustBeLoggedIn($app) {
            return function() use ($app) {
                if (!$app["session"]->get("user")) {
                    $app["session"]->invalidate();
                    return $app->abort("401", 'Unauthorized user');
                }
            };
        }

        public static function getMustBeArtist($app) {
            return function() use ($app) {
                $user = $app["session"]->get("user");
                $user = json_decode($user, true);
                if($user["rol"] != 'admin')
                if (!$user || $user["rol"] != 'artist' ) {
                    return $app->abort("401", 'Unauthorized artist');
                }
            };
        }

        public static function getMustBeFan($app) {
            return function() use ($app) {
                $user = $app["session"]->get("user");
                $user = json_decode($user, true);
                if($user["rol"] != 'admin')
                if (!$user || $user["rol"] != 'fan' ) {
                    return $app->abort("401", 'Unauthorized fan');
                }
            };
        }

        public static function getMustBeAnonymous($app) {
            return function() use ($app) {
                if ($app["session"]->get("user")) {
                    return $app->abort("401", 'Unauthorized not anonymous');
                }
            };
        }

        public static function notFound($app) {
            return function() use ($app) {
                return $app->abort("404", 'Not Found');
                
            };
        }

        public static function getMustBeAdmin($app) {
            return function() use ($app) {
                $admin = $app["session"]->get("user");
                $admin = json_decode($admin, true);
                if (!$admin || $admin["rol"] != 'admin') {
                    return $app->abort("401", 'Unauthorized admin');
                }
            };
        }

        public static function getMustBeAgent($app) {
            return function() use ($app) {
                $user = $app["session"]->get("user");
                $user = json_decode($user, true);
                if($user["rol"] != 'admin')
                if (!$user || $user["rol"] != 'agent' ) {
                    return $app->abort("401", 'Unauthorized agent');
                }
            };
        }

        public static function getMustBeShowSiteOwner($app) {
            return function() use ($app) {
                $user = $app["session"]->get("user");
                $user = json_decode($user, true);
                if($user["rol"] != 'admin')
                if (!$user || $user["rol"] != 'showSite') {
                    return $app->abort("401", 'Unauthorized owner');
                }
            };
        }
    }
}
?>
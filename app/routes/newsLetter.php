<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/newsLetters", "newsLetters.controller:create")
        ->bind("createnewsLetters")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/newsLetters", "newsLetters.controller:index")
        ->bind("newsLetters")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/newsLetters/{id}", "newsLetters.controller:show")
        ->bind("shownewsLetters")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/newsLetters/{id}", "newsLetters.controller:destroy")
        ->bind("destroynewsLetters")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/newsLetters/{id}", "newsLetters.controller:update")
        ->bind("updatenewsLetters")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));


?>

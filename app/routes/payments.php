<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/payments", "payments.controller:create")
        ->bind("createpayment")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/payments", "payments.controller:index")
        ->bind("payments")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));
        
$app->get("/payments/{id}", "payments.controller:show")
        ->bind("showpayment")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/payments/{id}", "payments.controller:destroy")
        ->bind("destroypayment")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/payments/{id}", "payments.controller:update")
        ->bind("updatepayment")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/dataaccess", "payments.controller:getDataAccess")
        ->bind("dataAccess")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->post("/createpreference", "payments.controller:createPreference")
        ->bind("createpreference")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));
?>

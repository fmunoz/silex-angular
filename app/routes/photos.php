<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/photos", "photos.controller:create")
        ->bind("createphoto")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeArtist($app));

$app->get("/photos", "photos.controller:index")
        ->bind("photos")
        ->before(App\Authorizations\Basic::getMustBeArtist($app));

$app->get("/photos/{id}", "photos.controller:show")
        ->bind("showphoto")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/photos/{id}", "photos.controller:destroy")
        ->bind("destroyphoto")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/photos/{id}", "photos.controller:update")
        ->bind("updatephoto")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

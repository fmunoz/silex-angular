<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/promos", "promos.controller:create")
        ->bind("createpromo")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/promos", "promos.controller:index")
        ->bind("promos")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/promos/byevent/{eventId}", "promos.controller:byEvent")
        ->bind("promos")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/promos/{id}", "promos.controller:show")
        ->bind("showpromo")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/promos/{id}", "promos.controller:destroy")
        ->bind("destroypromo")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/promos/{id}", "promos.controller:update")
        ->bind("updatepromo")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

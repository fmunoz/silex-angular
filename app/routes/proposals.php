<?php

$protectedRoutes = $app["controllers_factory"];


$app->get("/proposals/byagent", "proposals.controller:byAgent")
        ->bind("proposalsbyagent")
        ->before(App\Authorizations\Basic::getMustBeAgent($app));

$app->post("/proposals", "proposals.controller:create")
        ->bind("createproposal")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/proposals", "proposals.controller:index")
        ->bind("proposals")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/proposalsPublics", "proposals.controller:getProposalsPublics")
        ->bind("proposalsPublics")
        ->before(App\Authorizations\Basic::getMustBeFan($app));
        
$app->get("/proposals/{id}", "proposals.controller:show")
        ->bind("showproposal")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/proposals/{id}", "proposals.controller:destroy")
        ->bind("destroyproposal")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/proposals/{id}", "proposals.controller:update")
        ->bind("updateproposal")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/myProposalsArtist", "proposals.controller:myProposalsArtist")
        ->bind("myProposalsArtist")
        ->before(App\Authorizations\Basic::getMustBeArtist($app));


?>

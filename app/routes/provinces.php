<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/provinces", "provinces.controller:create")
        ->bind("createprovince")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/provinces", "provinces.controller:index")
        ->bind("provinces")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/provinces/{id}", "provinces.controller:show")
        ->bind("showprovince")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/provinces/{id}", "provinces.controller:destroy")
        ->bind("destroyprovince")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/provinces/{id}", "provinces.controller:update")
        ->bind("updateprovince")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/releases", "releases.controller:create")
        ->bind("createrelease")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/releases", "releases.controller:index")
        ->bind("releases")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/releases/{id}", "releases.controller:show")
        ->bind("showrelease")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/releases/{id}", "releases.controller:destroy")
        ->bind("destroyrelease")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/releases/{id}", "releases.controller:update")
        ->bind("updaterelease")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));


?>

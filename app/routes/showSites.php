<?php

$protectedRoutes = $app["controllers_factory"];


$app->get("/showSitesByUser", "showSites.controller:getShowSiteByUser")
        ->bind("showSitesByUser")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/showSitesPublics", "showSites.controller:getShowSitesPublics")
        ->bind("showSitesPublics")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->post("/showSites", "showSites.controller:create")
        ->bind("createshowSite")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeShowSiteOwner($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/showSites", "showSites.controller:index")
        ->bind("showSites")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/showSites/{id}", "showSites.controller:show")
        ->bind("showshowSite")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/showSites/{id}", "showSites.controller:destroy")
        ->bind("destroyshowSite")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/showSites/{id}", "showSites.controller:update")
        ->bind("updateshowSite")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeShowSiteOwner($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/tickets", "tickets.controller:create")
        ->bind("createticket")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/tickets", "tickets.controller:index")
        ->bind("tickets")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/tickets/{id}", "tickets.controller:show")
        ->bind("showticket")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/ticketsFan", "tickets.controller:getTicketFan")
        ->bind("showticketFan")
        ->before(App\Authorizations\Basic::getMustBeFan($app));

$app->delete("/tickets/{id}", "tickets.controller:destroy")
        ->bind("destroyticket")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/tickets/{id}", "tickets.controller:update")
        ->bind("updateticket")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));


?>

<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/userNewsLetters", "userNewsLetters.controller:create")
        ->bind("createuserNewsLetter")
        ->before(App\REST\Basic::mustBeValidJSON($app));        

$app->get("/userNewsLetters", "userNewsLetters.controller:index")
        ->bind("userNewsLetters")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/userNewsLetters/{id}", "userNewsLetters.controller:show")
        ->bind("showuserNewsLetters")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->delete("/userNewsLetters/{id}", "userNewsLetters.controller:destroy")
        ->bind("destroyuserNewsLetters")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->put("/userNewsLetters/{id}", "userNewsLetters.controller:update")
        ->bind("updateuserNewsLetters")
        ->before(App\REST\Basic::mustBeValidJSON($app));


?>

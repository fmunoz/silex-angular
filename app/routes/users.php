<?php

$app->get("/loggedin", "users.controller:loggedin")
        ->bind("loggedin");

$app->get("/logout", "users.controller:logout")
        ->bind("logout");

$app->post("/login", "users.controller:login")
        ->bind("login")
        ->before(App\Authorizations\Basic::getMustBeAnonymous($app));

$app->post("/register", "users.controller:create")
        ->bind("registeruser")
        ->before(App\Authorizations\Basic::getMustBeAnonymous($app));

$app->delete("/users/{id}", "users.controller:destroy")
        ->bind("destroyuser");
        //para testear se utilizó esta función y en la app real
        //tal vez haga falta que haya un usuario ADMIN loggeado

$app->get("/getArtists", "users.controller:getArtists")
        ->bind("usersArtists")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/users", "users.controller:index")
        ->bind("users")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/getPublicArtists", "users.controller:getPublicArtists")
        ->bind("usersPublicArtists")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/getFans", "users.controller:getFans")
        ->bind("usersFans")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/getShowSitesUsers", "users.controller:getShowSitesUsers")
        ->bind("usersShowSites")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/getAgents", "users.controller:getAgents")
        ->bind("usersAgents")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/getUser/{id}", "users.controller:show")
        ->bind("getUser")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/getLoginFacebookUrl", "users.controller:getLoginFacebookUrl")
        ->bind("getFacebookLoginUrl");

$app->get("/getPreDataFacebook", "users.controller:getPreDataFacebook")
        ->bind("getPreDataFacebook");

$app->get("/getDataFacebook", "users.controller:getDataFacebook")
        ->bind("getDataFacebook");

$app->post("/loginFan", "users.controller:loginFan")
        ->bind("loginfan")
        ->before(App\Authorizations\Basic::getMustBeAnonymous($app));
        
$app->post("/registerFan", "users.controller:registerFan")
        ->bind("registeruserfan")
        ->before(App\Authorizations\Basic::getMustBeAnonymous($app));

?>
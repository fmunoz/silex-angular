<?php

$protectedRoutes = $app["controllers_factory"];


$app->post("/votes", "votes.controller:create")
        ->bind("createvote")
        ->before(App\REST\Basic::mustBeValidJSON($app))
        ->before(App\Authorizations\Basic::getMustBeFan($app));

$app->get("/votesbyartist", "votes.controller:getVotesArtist")
        ->bind("votesbyartist")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/votesbyoneartist", "votes.controller:getVotesOneArtist")
        ->bind("votesbyoneartist")
        ->before(App\Authorizations\Basic::getMustBeLoggedIn($app));

$app->get("/votes", "votes.controller:index")
        ->bind("votes")
        ->before(App\Authorizations\Basic::getMustBeAdmin($app));

$app->get("/votes/{id}", "votes.controller:show")
        ->bind("showvote")
        ->before(App\Authorizations\Basic::notFound($app));

$app->delete("/votes/{id}", "votes.controller:destroy")
        ->bind("destroyvote")
        ->before(App\Authorizations\Basic::getMustBeFan($app));

$app->put("/votes/{id}", "votes.controller:update")
        ->bind("updatevote")
        ->before(App\REST\Basic::mustBeValidJSON($app));
        //->before(App\Authorizations\Basic::notFound($app));
?>

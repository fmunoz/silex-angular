<?php

//Admins
$app["admins.entity"] = function($app) {
    return new App\Entity\Admin();
};

$app["admins.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Admin");
};

$app["admins.manager"] = function($app) {
    return new App\BusinessLogic\AdminsLogic($app["admins.repository"], $app["admins.entity"], $app["db.orm.em"], $app["session"]);
};

$app['admins.controller'] = function($app)  {
    return new App\Controller\AdminsCtr($app['admins.manager']);
};

?>
<?php

//Cities
$app["cities.entity"] = function($app) {
    return new App\Entity\City();
};

$app["cities.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\City");
};

$app["cities.manager"] = function($app) {
    return new App\BusinessLogic\CitiesLogic($app["cities.repository"], $app["cities.entity"], $app["db.orm.em"], $app["session"]);
};

$app['cities.controller'] = function($app)  {
    return new App\Controller\CitiesCtr($app['cities.manager']);
};

?>
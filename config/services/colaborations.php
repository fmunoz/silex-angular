<?php

//Admins
$app["colaborations.entity"] = function($app) {
    return new App\Entity\Colaboration();
};

$app["colaborations.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Colaboration");
};

$app["colaborations.manager"] = function($app) {
    return new App\BusinessLogic\ColaborationsLogic($app["colaborations.repository"], $app["colaborations.entity"], $app["db.orm.em"], $app["session"]);
};

$app['colaborations.controller'] = function($app)  {
    return new App\Controller\ColaborationsCtr($app['colaborations.manager']);
};

?>
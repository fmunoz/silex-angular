<?php

//Events
$app["events.entity"] = function($app) {
    return new App\Entity\Event();
};

$app["events.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Event");
};

$app["events.manager"] = function($app) {
    return new App\BusinessLogic\EventsLogic($app["events.repository"], $app["events.entity"], $app["db.orm.em"], $app["session"]);
};

$app['events.controller'] = function($app)  {
    return new App\Controller\EventsCtr($app['events.manager']);
};

?>
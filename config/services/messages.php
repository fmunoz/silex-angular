<?php

//Messages
$app["messages.entity"] = function($app) {
    return new App\Entity\Message();
};

$app["messages.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Message");
};

$app["messages.manager"] = function($app) {
    return new App\BusinessLogic\MessagesLogic($app["messages.repository"], $app["messages.entity"], $app["db.orm.em"], $app["session"]);
};

$app['messages.controller'] = function($app)  {
    return new App\Controller\MessagesCtr($app['messages.manager']);
};

?>
<?php

//Releases
$app["newsLetters.entity"] = function($app) {
    return new App\Entity\NewsLetter();
};

$app["newsLetters.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\NewsLetter");
};

$app["newsLetters.manager"] = function($app) {
    return new App\BusinessLogic\NewsLettersLogic($app["newsLetters.repository"], $app["newsLetters.entity"], $app["db.orm.em"], $app["session"]);
};

$app['newsLetters.controller'] = function($app)  {
    return new App\Controller\NewsLettersCtr($app['newsLetters.manager']);
};

?>
<?php

//Payments
$app["payments.entity"] = function($app) {
    return new App\Entity\Payment();
};

$app["payments.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Payment");
};

$app["payments.manager"] = function($app) {
    return new App\BusinessLogic\PaymentsLogic($app["payments.repository"], $app["payments.entity"], $app["db.orm.em"], $app["session"], $app["mp"]);
};

$app['payments.controller'] = function($app) {
    return new App\Controller\PaymentsCtr($app['payments.manager']);
};

?>
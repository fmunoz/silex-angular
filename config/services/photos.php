<?php

//Photos
$app["photos.entity"] = function($app) {
    return new App\Entity\Photo();
};

$app["photos.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Photo");
};

$app["photos.manager"] = function($app) {
    return new App\BusinessLogic\PhotosLogic($app["photos.repository"], $app["photos.entity"], $app["db.orm.em"], $app["session"]);
};

$app['photos.controller'] = function($app)  {
    return new App\Controller\PhotosCtr($app['photos.manager']);
};

?>
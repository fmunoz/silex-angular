<?php

//Promos
$app["promos.entity"] = function($app) {
    return new App\Entity\Promo();
};

$app["promos.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Promo");
};

$app["promos.manager"] = function($app) {
    return new App\BusinessLogic\PromosLogic($app["promos.repository"], $app["promos.entity"], $app["db.orm.em"], $app["session"]);
};

$app['promos.controller'] = function($app)  {
    return new App\Controller\PromosCtr($app['promos.manager']);
};

?>
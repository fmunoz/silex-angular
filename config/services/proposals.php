<?php

//Proposals
$app["proposals.entity"] = function($app) {
    return new App\Entity\Proposal();
};

$app["proposals.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Proposal");
};

$app["proposals.manager"] = function($app) {
    return new App\BusinessLogic\ProposalsLogic($app["proposals.repository"], $app["proposals.entity"], $app["db.orm.em"], $app["session"]);
};

$app['proposals.controller'] = function($app)  {
    return new App\Controller\ProposalsCtr($app['proposals.manager']);
};

?>
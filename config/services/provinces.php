<?php

//Provinces
$app["provinces.entity"] = function($app) {
    return new App\Entity\Province();
};

$app["provinces.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Province");
};

$app["provinces.manager"] = function($app) {
    return new App\BusinessLogic\ProvincesLogic($app["provinces.repository"], $app["provinces.entity"], $app["db.orm.em"], $app["session"]);
};

$app['provinces.controller'] = function($app) {
    return new App\Controller\ProvincesCtr($app['provinces.manager']);
};

?>
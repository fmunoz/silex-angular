<?php

//Releases
$app["releases.entity"] = function($app) {
    return new App\Entity\Release();
};

$app["releases.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Release");
};

$app["releases.manager"] = function($app) {
    return new App\BusinessLogic\ReleasesLogic($app["releases.repository"], $app["releases.entity"], $app["db.orm.em"], $app["session"]);
};

$app['releases.controller'] = function($app)  {
    return new App\Controller\ReleasesCtr($app['releases.manager']);
};

?>
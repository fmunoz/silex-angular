<?php

//showSites
$app["showSites.entity"] = function($app) {
    return new App\Entity\ShowSite();
};

$app["showSites.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\ShowSite");
};

$app["showSites.manager"] = function($app) {
    return new App\BusinessLogic\ShowSitesLogic($app["showSites.repository"], $app["showSites.entity"], $app["db.orm.em"], $app["session"]);
};

$app['showSites.controller'] = function($app)  {
    return new App\Controller\ShowSitesCtr($app['showSites.manager']);
};

?>
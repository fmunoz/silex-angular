<?php

//Tickets
$app["tickets.entity"] = function($app) {
    return new App\Entity\Ticket();
};

$app["tickets.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Ticket");
};

$app["tickets.manager"] = function($app) {
    return new App\BusinessLogic\TicketsLogic($app["tickets.repository"], $app["tickets.entity"], $app["db.orm.em"], $app["session"]);
};

$app['tickets.controller'] = function($app)  {
    return new App\Controller\TicketsCtr($app['tickets.manager']);
};

?>
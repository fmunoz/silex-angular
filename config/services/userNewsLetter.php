<?php

//Releases
$app["userNewsLetters.entity"] = function($app) {
    return new App\Entity\UserNewsLetter();
};

$app["userNewsLetters.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\UserNewsLetter");
};

$app["userNewsLetters.manager"] = function($app) {
    return new App\BusinessLogic\UserNewsLettersLogic($app["userNewsLetters.repository"], $app["userNewsLetters.entity"], $app["db.orm.em"], $app["session"]);
};

$app['userNewsLetters.controller'] = function($app)  {
    return new App\Controller\UserNewsLettersCtr($app['userNewsLetters.manager']);
};

?>
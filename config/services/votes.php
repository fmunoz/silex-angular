<?php

//Votes
$app["votes.entity"] = function($app) {
    return new App\Entity\Vote();
};

$app["votes.repository"] = function($app) {
    return $app["db.orm.em"]->getRepository("App\Entity\Vote");
};

$app["votes.manager"] = function($app) {
    return new App\BusinessLogic\VotesLogic($app["votes.repository"], $app["votes.entity"], $app["db.orm.em"], $app["session"]);
};

$app['votes.controller'] = function($app)  {
    return new App\Controller\VotesCtr($app['votes.manager']);
};

?>
'use strict';

angular.module('mean.controllers.login', [])
    .controller('LoginCtrl', ['$scope','$rootScope','$http','$location', function($scope, $rootScope, $http, $location) {
        // This object will be filled by the form
        $scope.user = {};

        // Register the login() function
        $scope.login = function(){

            if($scope.isAdmin){
                $http.post('/loginAdmin', {
                    email: $scope.user.email,
                    password: $scope.user.password
                })
                    .success(function(user){
                        // authentication OK
                        $scope.loginError = 0;
                        $rootScope.user = user;
                        $rootScope.$emit('loggedin');
                        $location.url('/');
                    })
                    .error(function() {
                        $scope.loginerror = 'Authentication failed.';
                    });
            }
            else{
                $http.post('/login', {
                    email: $scope.user.email,
                    password: $scope.user.password
                })
                    .success(function(user){
                        // authentication OK
                        $scope.loginError = 0;
                        $rootScope.user = user;
                        $rootScope.$emit('loggedin');
                        $location.url('/');
                    })
                    .error(function() {
                        $scope.loginerror = 'Authentication failed.';
                    });
            }
        };
        // Register the login() function
        $scope.logout = function(){
            $http.get('/logout', {
            })
                .success(function(data){
                    // logout OK
                    $rootScope.user = null;
                    $rootScope.$emit('loggedin');
                    $location.url('/');

                })
                .error(function() {
                    $scope.logouterror = 'Logout failed.';
                });
        };

        $scope.faceLog = function() {
            $http.get('/getLoginFacebookUrl').success(function(data){
                $scope.loginUrl = data;
            });

        };

        $scope.goLogin = function() {
            $http.get('/getLoginFacebookUrl');
        };

        $scope.tryLoginFacebook = function() {
            $http.get('/getDataFacebook')
            .success(function(data){
                $scope.userFb = data;
                $http.post('/registerFan',{
                    email: $scope.userFb.id + "@user.com",
                    idFacebook: $scope.userFb.id,
                    name: $scope.userFb.first_name,
                    surname: $scope.userFb.last_name
                })
                .success(function(user){
                    $scope.userFb = user;
                    $http.post('/loginFan',{
                        idFacebook: $scope.userFb.idFacebook,
                    })
                    .success(function(user){
                        // authentication system OK
                        $scope.loginError = 0;
                        $rootScope.user = user;
                        $rootScope.$emit('loggedin');
                        $location.url('/');
                    })
                    .error(function() {
                        $scope.loginerror = 'Authentication failed.';
                    });
                })
                .error(function() {
                    $scope.loginerror = 'Authentication failed.';
                });
            })
            .error(function() {
                $scope.loginerror = 'Authentication failed.';
            });
        };

    }])
    .controller('RegisterCtrl', ['$scope','$rootScope','$http','$location', function($scope, $rootScope, $http, $location) {
        $scope.user = {};

        $scope.register = function(){
            $scope.usernameError = null;
            $scope.registerError = null;

            if($scope.rol == 'artist'){
                $http.post('/register',{
                    email : $scope.email,
                    name : $scope.name,
                    surname : $scope.surname,
                    password : $scope.confirmPassword,
                    artisticName : $scope.artisticName,
                    rol : $scope.rol,
                    phone : $scope.phone
                })
                .success(function(){
                    $location.url('/');
                })
                .error(function(){
                    $scope.usernameError = "Error";
                });
            }
            else{
                $http.post('/register',{
                    email : $scope.email,
                    name : $scope.name,
                    surname : $scope.surname,
                    password : $scope.confirmPassword,
                    rol : $scope.rol,
                    phone : $scope.phone
                })
                .success(function(){
                    $location.url('/');
                })
                .error(function(){
                    $scope.usernameError = "Error";
                });   
            }

        };
        
    }]);

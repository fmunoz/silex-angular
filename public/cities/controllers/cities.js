'use strict';

angular.module('mean.cities').controller('CitiesController', ['$scope', '$stateParams', '$location', 'Global', 'Cities', function ($scope, $stateParams, $location, Global, Cities) {
    $scope.global = Global;

    $scope.create = function() {

        var provinceId = $stateParams.provinceId;
        var city = new Cities({
            name: this.name,
            province: provinceId
        });
        city.$save(function(response) {
            $location.path('cities/' + response.id);
        });

        this.name = '';
    };

    $scope.remove = function(city) {
        if (city) {
            city.$remove();

            for (var i in $scope.cities) {
                if ($scope.cities[i] === city) {
                    $scope.cities.splice(i, 1);
                }
            }
        }
        else {
            $scope.city.$remove();
            $location.path('cities');
        }
    };

    $scope.update = function() {
        var city = $scope.city;
        city.$update(function() {
            $location.path('cities/' + city.id);
        });
    };

    $scope.find = function() {
        Cities.query(function(cities) {
            $scope.cities = cities;
        });
    };

    $scope.findOne = function() {
        Cities.get({
            cityId: $stateParams.cityId
        }, function(city) {
            $scope.city = city;
        });
    };

    $scope.findByProvince = function() {
        Cities.byProvince({
            provinceId: $stateParams.provinceId
        }, function(cities) {
            $scope.cities = cities;
        });
    };
}]);
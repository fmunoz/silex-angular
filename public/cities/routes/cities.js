'use strict';

//Setting up route
angular.module('mean.cities').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('all cities by province', {
                url: '/provinces/:provinceId/cities',
                templateUrl: 'public/cities/views/list.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create city', {
                url: '/provinces/:provinceId/cities/create',
                templateUrl: 'public/cities/views/create.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('edit city', {
                url: '/cities/:cityId/edit',
                templateUrl: 'public/cities/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('city by id', {
                url: '/cities/:cityId',
                templateUrl: 'public/cities/views/view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);

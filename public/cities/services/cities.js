'use strict';

//Cities service used for cities REST endpoint
angular.module('mean.cities').factory('Cities', ['$resource', function($resource) {
    return $resource('cities/:cityId/:action/:provinceId', {
        cityId: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        byProvince: { 'method': 'GET', 'params': {'provinceId': "true", 'action': "byprovince"}, isArray: true }
    });
}]);
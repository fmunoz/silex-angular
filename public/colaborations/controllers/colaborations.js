'use strict';

// Colaborations controller
angular.module('mean.colaborations').controller('ColaborationsController', ['$scope', '$stateParams', '$location', 'Global', 'Colaborations',
	function($scope, $stateParams, $location, Global, Colaborations ) {
		$scope.global = Global;

		// Create new Colaboration
		$scope.create = function() {
			// Create new Colaboration object
			var colaboration = new Colaborations ({
				mount: this.mount,
				proposal: $stateParams.proposalId
			});

			// Redirect after save
			colaboration.$save(function(response) {
				$location.path('colaborations/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
		};

		// Remove existing Colaboration
		$scope.remove = function( colaboration ) {
			if ( colaboration ) { colaboration.$remove();

				for (var i in $scope.colaborations ) {
					if ($scope.colaborations [i] === colaboration ) {
						$scope.colaborations.splice(i, 1);
					}
				}
			} else {
				$scope.colaboration.$remove(function() {
					$location.path('colaborations');
				});
			}
		};

		// Update existing Colaboration
		$scope.update = function() {
			var colaboration = $scope.colaboration ;

			colaboration.$update(function() {
				$location.path('colaborations/' + colaboration.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Colaborations
		$scope.find = function() {
			$scope.colaborations = Colaborations.query();
		};

		// Find existing Colaboration
		$scope.findOne = function() {
			$scope.colaboration = Colaborations.get({ 
				colaborationId: $stateParams.colaborationId
			});
		};

		$scope.createPreference = function() {
			$http.post('/createpreference',{
                price: this.mount,
				concept: "saldo",
				state: "in_mediation"
            })
            .success(function(preference){
                $scope.preference = preference;
                $location.path('colaborations/payment');
            })
		};		
	}
]);
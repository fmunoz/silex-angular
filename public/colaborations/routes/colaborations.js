'use strict';

//Setting up route
angular.module('mean.colaborations').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Colaborations state routing
		$stateProvider.
		state('listColaborations', {
			url: '/colaborations',
			templateUrl: 'public/colaborations/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createColaboration', {
			url: '/proposals/:proposalId/colaborations/create',
			templateUrl: 'public/colaborations/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewColaboration', {
			url: '/colaborations/:colaborationId',
			templateUrl: 'public/colaborations/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editColaboration', {
			url: '/colaborations/:colaborationId/edit',
			templateUrl: 'public/colaborations/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
'use strict';

//Colaborations service used to communicate Colaborations REST endpoints
angular.module('mean.colaborations').factory('Colaborations', ['$resource',
	function($resource) {
		return $resource('colaborations/:colaborationId', { colaborationId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
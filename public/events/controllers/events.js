'use strict';

// Events controller
angular.module('mean.events').controller('EventsController', ['$scope', '$stateParams', '$location', 'Global', 'Events', 'Proposals', '$http',
	function($scope, $stateParams, $location, Global, Events , Proposal, $http) {
		$scope.global = Global;

    	// Create new Event
		$scope.create = function() {
			// Create new Event object
			var event = new Events ({
				dateInit : this.proposal.dateInit,
				deadLine : this.proposal.deadLine,
				colaborationsQuantity : this.proposal.colaborationsQuantity,
				picture : this.proposal.picture,
				showSiteName : this.proposal.showSiteName,
				showSiteAddres : this.proposal.showSiteAddres,
				state : "Publico",
				province : this.proposal.province,
				city : this.proposal.city,
				artist : this.proposal.artist,
				ticketsAvailablesQuantity: this.ticketsAvailablesQuantity,
				ticketsSoldsQuantity: this.ticketsSoldsQuantity,
				ticketPrice: this.ticketPrice,
			});
			if (this.proposal.agent != null)
				event.agent = this.proposal.agent;
			if (this.proposal.showSite != null)
				event.showSite = this.proposal.showSite;

			// Redirect after save
			event.$save(function(response) {
				$location.path('events/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
			//update proposals
			this.proposal.state = "Evento";
			Proposal(this.proposal.id,this.proposal);
			console.log(this.proposal);

		};

		// Remove existing Event
		$scope.remove = function( event ) {
			if ( event ) { event.$remove();

				for (var i in $scope.events ) {
					if ($scope.events [i] === event ) {
						$scope.events.splice(i, 1);
					}
				}
			} else {
				$scope.event.$remove(function() {
					$location.path('events');
				});
			}
		};

		// Update existing Event
		$scope.update = function() {
			var event = $scope.event ;

			event.$update(function() {
				$location.path('events/' + event.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Events
		$scope.find = function() {
			$scope.events = Events.query();
		};

		// Find existing Event
		$scope.findOne = function() {
			$scope.event = Events.get({ 
							eventId: $stateParams.eventId
						});
		};

		$scope.cancelEvent = function() {
        	var event = $scope.event ;
        	event.state = "Cancelado";
			event.$update(function() {
				$location.path('events/' + event.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
    	};

		$scope.createPromo = function() {
        	$location.path('events/'+ $scope.event.id +'/promos/create') 
    	};

    	$scope.viewPromo = function() {
        	$location.path('events/'+ $scope.event.id +'/promos') 
    	};

		$scope.getProposal = function() {
			$scope.proposal = Proposal.get({ 
				proposalId: $stateParams.proposalId
			});
		};

		$scope.byArtist = function(){
            $http.get('/events/byartist').success(function(data) {
                $scope.eventsByArtist = data;
            });
        };

        $scope.findPublic = function(){
        	$http.get('/eventsPublics').success(function(data) {
                $scope.events = data;
            });
        };

        $scope.findUser = function(id){
        	$http.get('/getUser/'+id).success(function(data) {
                $scope.foundUser = data;
            });
        };

        $scope.findProvince = function(id){
        	$http.get('/provinces/'+id).success(function(data) {
                $scope.foundProvince = data;
            });
        };

        $scope.findCity = function(id){
        	$http.get('/cities/'+id).success(function(data) {
                $scope.foundCity = data;
            });
        };
	}
]);
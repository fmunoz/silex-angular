'use strict';

//Setting up route
angular.module('mean.events').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Events state routing
		$stateProvider.
		state('byartist', {
            url: '/events/byartist',
            templateUrl: 'public/events/views/byartist.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).state('listEvents', {
			url: '/events',
			templateUrl: 'public/events/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
        state('listEventsPublics', {
            url: '/eventsPublics',
            templateUrl: 'public/events/views/listPublic.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('createEvent', {
            url: '/proposals/:proposalId/events/create',
            templateUrl: 'public/events/views/create.html',
            resolve: {
                    loggedin: checkLoggedin
            }
        }).
		state('viewEvent', {
			url: '/events/:eventId',
			templateUrl: 'public/events/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editEvent', {
			url: '/events/:eventId/edit',
			templateUrl: 'public/events/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
'use strict';

//Events service used to communicate Events REST endpoints
angular.module('mean.events').factory('Events', ['$resource',
	function($resource) {
		return $resource('events/:eventId', { eventId: '@id'
		}, {
			update: {
				method: 'PUT'
			},
			getProposal: { 'method': 'GET', 'params': {'get': "true", 'action': "get"}, isArray: false }
		});
	}
]);
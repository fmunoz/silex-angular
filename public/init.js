'use strict';

angular.element(document).ready(function() {
    //Fixing facebook bug with redirect
    if (window.location.hash === '#_=_') window.location.hash = '#!';

    //Then init the app
    angular.bootstrap(document, ['mean']);

});

// Dynamically add angular modules declared by packages
var packageModules = [];
for (var index in window.modules) {
    angular.module(window.modules[index].module, window.modules[index].angularDependencies || []);
    packageModules.push(window.modules[index].module);
}

// Default modules
var modules = ['ngCookies', 'ngResource','ui.bootstrap', 'ui.router', 'mean.system', 'mean.articles', 'mean.auth', 'mean.provinces', 'mean.cities', 'mean.proposals','mean.events','mean.photos','mean.colaborations','mean.promos', 'mean.messages', 'mean.showsites', 'mean.users', 'mean.tickets', 'mean.votes', 'mean.releases'];

modules = modules.concat(packageModules);

// Combined modules
angular.module('mean', modules);

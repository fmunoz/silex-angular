'use strict';

// Messages controller
angular.module('mean.messages').controller('MessagesController', ['$scope', '$stateParams', '$location', 'Global', 'Messages', 'Users', '$http',
	function($scope, $stateParams, $location, Global, Messages, Users, $http ) {
		$scope.global = Global;

		$scope.preCreate = function(){
			$scope.userReceiver = $stateParams.idUser;
			
		};

		$scope.loadUsers = function() {
			$scope.users = Users.query();
		};

		// Create new Message
		$scope.create = function() {
			// Create new Message object
			var message = new Messages ({
				subject: this.subject,
				content: this.content,
				userReceiver: $scope.userReceiver,
				date: "now",
				state: "No leido"
			});

			// Redirect after save
			message.$save(function(response) {
				$location.path('messages/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
			
		};

		// Remove existing Message
		$scope.remove = function( message ) {
			if ( message ) { message.$remove();

				for (var i in $scope.messages ) {
					if ($scope.messages [i] === message ) {
						$scope.messages.splice(i, 1);
					}
				}
			} else {
				$scope.message.$remove(function() {
					$location.path('messages');
				});
			}
		};

		// Update existing Message
		$scope.update = function() {
			var message = $scope.message ;

			message.$update(function() {
				$location.path('messages/' + message.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Messages
		$scope.find = function() {
			$scope.messages = Messages.query();
		};

		// Find existing Message
		$scope.findOne = function() {
			Messages.get({ 
				messageId: $stateParams.messageId
			}, 	function(message){
					$scope.message = message;
					if(message.userReceiver.id == $scope.global.user.id)
					{
						//var message = $scope.message ;
						message.state = "Leido";
						message.$update();	
					}
				}
			);
		};


		//this replace find original
		$scope.preList = function() {
			$scope.getMessagesSent();
			$scope.getMessagesReceived();
		};

		$scope.getMessagesReceived = function() {
			$http.get('/messagesReceived').success(function(data) {
        		$scope.messagesReceived = data;
        	});
		
		};

		$scope.getMessagesSent = function() {
			$http.get('/messagesSent').success(function(data) {
        		$scope.messagesSent = data;
        	});
		};

		$scope.findUser = function(id){
			console.log(id);
        	$http.get('/getUser/'+id).success(function(data) {
                $scope.foundUser = data;
            });
        };


	}
]);
'use strict';

//Setting up route
angular.module('mean.messages').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Messages state routing
		$stateProvider.
		state('listMessages', {
			url: '/messages',
			templateUrl: 'public/messages/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createMessage', {
			url: '/messages/create/:idUser',
			templateUrl: 'public/messages/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewMessage', {
			url: '/messages/:messageId',
			templateUrl: 'public/messages/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editMessage', {
			url: '/messages/:messageId/edit',
			templateUrl: 'public/messages/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
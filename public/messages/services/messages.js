'use strict';

//Messages service used to communicate Messages REST endpoints
angular.module('mean.messages').factory('Messages', ['$resource',
	function($resource) {
		return $resource('messages/:messageId', { messageId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
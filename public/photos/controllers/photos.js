'use strict';

// Photos controller
angular.module('mean.photos', ['ngUpload']).controller('PhotosController', ['$scope', '$stateParams', '$location', 'Global', 'Photos',
	function($scope, $stateParams, $location, Global, Photos ) {
		$scope.global = Global;

		// Create new Photo
		$scope.create = function() {
			
			var file = this.photo;

			console.log(file);


			// Create new Photo object
			var photo = new Photos ({
				path: "nueva imagen"
			});

			// Redirect after save
			photo.$save(function(response) {
				$location.path('photos/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.path = '';
		};

		// Remove existing Photo
		$scope.remove = function( photo ) {
			if ( photo ) { photo.$remove();

				for (var i in $scope.photos ) {
					if ($scope.photos [i] === photo ) {
						$scope.photos.splice(i, 1);
					}
				}
			} else {
				$scope.photo.$remove(function() {
					$location.path('photos');
				});
			}
		};

		// Find a list of Photos
		$scope.find = function() {
			$scope.photos = Photos.query();
		};

		// Find existing Photo
		$scope.findOne = function() {
			$scope.photo = Photos.get({ 
				photoId: $stateParams.photoId
			});
		};


    	$scope.complete = function(content) {
      		console.log("aca estoy"); // process content
    	};

    	$scope.uploadComplete = function (content) {
		    $scope.response = JSON.parse(content); // Presumed content is a json string!
			console.log($scope.response);
		    // Look for way to clear the input[type=file] element
		};

		$scope.view = function() {
			
			var file = this.file;

			//console.log(file);
		};
		
	}
]);
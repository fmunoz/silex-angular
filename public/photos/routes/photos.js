'use strict';

//Setting up route
angular.module('mean.photos').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Photos state routing
		$stateProvider.
		state('listPhotos', {
			url: '/photos',
			templateUrl: 'public/photos/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createPhoto', {
			url: '/photos/create',
			templateUrl: 'public/photos/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewPhoto', {
			url: '/photos/:photoId',
			templateUrl: 'public/photos/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editPhoto', {
			url: '/photos/:photoId/edit',
			templateUrl: 'public/photos/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
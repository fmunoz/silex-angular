'use strict';

//Photos service used to communicate Photos REST endpoints
angular.module('mean.photos').factory('Photos', ['$resource',
	function($resource) {
		return $resource('photos/:photoId', { photoId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
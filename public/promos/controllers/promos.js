'use strict';

// Promos controller
angular.module('mean.promos').controller('PromosController', ['$scope', '$stateParams', '$location', 'Global', 'Promos',
	function($scope, $stateParams, $location, Global, Promos ) {
		$scope.global = Global;

		// Create new Promo
		$scope.create = function() {
			// Create new Promo object
			var promo = new Promos ({
				discount: this.discount,
				description: this.description,
				price: this.price,
				event: $stateParams.eventId
			});

			// Redirect after save
			promo.$save(function(response) {
				$location.path('promos/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.discount = '';
			this.description = '';
			this.price = '';
		};

		// Remove existing Promo
		$scope.remove = function( promo ) {
			if ( promo ) { promo.$remove();

				for (var i in $scope.promos ) {
					if ($scope.promos [i] === promo ) {
						$scope.promos.splice(i, 1);
					}
				}
			} else {
				$scope.promo.$remove(function() {
					$location.path('promos');
				});
			}
		};

		// Update existing Promo
		$scope.update = function() {
			var promo = $scope.promo ;

			promo.$update(function() {
				$location.path('promos/' + promo.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Promos
		$scope.find = function() {
			$scope.promos = Promos.query();
		};

		// Find existing Promo
		$scope.findOne = function() {
			$scope.promo = Promos.get({ 
				promoId: $stateParams.promoId
			});
		};

		$scope.findByEvent = function() {
	        Promos.byEvent({
	            eventId: $stateParams.eventId
	        }, function(promos) {
	            $scope.promos = promos;
        	});
    	};
	}
]);
'use strict';

//Setting up route
angular.module('mean.promos').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Promos state routing
		$stateProvider.
        state('listPromosByEvent', {
            url: '/events/:eventId/promos',
            templateUrl: 'public/promos/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('createPromo', {
            url: '/events/:eventId/promos/create',
            templateUrl: 'public/promos/views/create.html',
            resolve: {
                    loggedin: checkLoggedin
            }
        }).
		state('viewPromo', {
			url: '/promos/:promoId',
			templateUrl: 'public/promos/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editPromo', {
			url: '/promos/:promoId/edit',
			templateUrl: 'public/promos/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
'use strict';

//Promos service used to communicate Promos REST endpoints
angular.module('mean.promos').factory('Promos', ['$resource',
	function($resource) {
		return $resource('promos/:promoId/:action/:eventId', { promoId: '@id'
		}, {
			update: {
				method: 'PUT'
			},
        	byEvent: { 'method': 'GET', 'params': {'eventId': "true", 'action': "byevent"}, isArray: true }
		});
	}
]);
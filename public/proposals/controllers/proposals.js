'use strict';

// Proposals controller
angular.module('mean.proposals').controller('ProposalsController', ['$scope', '$stateParams', '$location', 'Global', 'Proposals', 'Provinces', 'Cities','$http',
	function($scope, $stateParams, $location, Global, Proposals, Provinces, Cities, $http) {
		$scope.global = Global;

		$scope.open = function($event,openedParam) {
			$event.preventDefault();
			$event.stopPropagation();
			switch (openedParam) { 
			   	case "opened": 
			 		$scope.opened = true;
			      	break 
			   	case "opened2": 
			      	$scope.opened2 = true;
			      	break 
			   	default: break
			}
		  };


		// Create new Proposal
		$scope.create = function() {
			// Create new Proposal object

			event.preventDefault();
			var proposal = new Proposals ({
			dateInit : this.dateInit,
			deadLine : this.deadLine,
			colaborationsQuantity : this.colaborationsQuantity,
			picture : this.picture,
			showSiteName : this.showSiteName,
			showSiteAddres : this.showSiteAddres,
			province : this.province,
			city : this.city,
			artist : this.artist
			});
			
		
			if(this.typeSS != "new"){
				var showS;
				var ss;
				var i;
				for(i=0; i < $scope.showSites.length; i++){
					if($scope.showSites[i].id == this.showSite)
						showS = $scope.showSites[i];
				}

				proposal.showSite = this.showSite;
				proposal.showSiteName = showS.name;
				proposal.showSiteAddres = showS.addres;
			}
			else{
				proposal.showSiteName = this.showSiteName;
				proposal.showSiteAddres = this.showSiteAddres;
			}

			if($scope.global.user.rol!="artist"){
				proposal.state = "Pre-Revision";
				proposal.agent = $scope.global.user;
			}
			else
				proposal.state = "Revision";



			// Redirect after save
			proposal.$save(function(response) {
				$location.path('proposals/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
		};

		// Remove existing Proposal
		$scope.remove = function( proposal ) {
			if ( proposal ) { proposal.$remove();

				for (var i in $scope.proposals ) {
					if ($scope.proposals [i] === proposal ) {
						$scope.proposals.splice(i, 1);
					}
				}
			} else {
				$scope.proposal.$remove(function() {
					$location.path('proposals');
				});
			}
		};

		// Update existing Proposal
		$scope.update = function() {
			var proposal = $scope.proposal ;

			console.log(proposal);
			
			proposal.$update(function() {
				$location.path('proposals/' + proposal.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Proposals
		function extraData() {
			angular.forEach($scope.proposals,function(proposal){
				$http.get('/colaborations/total/'+proposal.id).success(function(data) {
			       proposal["approved"] = (parseInt(data.total) >= parseInt(proposal.colaborationsQuantity) && proposal.state != "Evento");
			    });
			});
		}

		$scope.find = function() {
			Proposals.query(function(proposals) {
		    	$scope.proposals = proposals;
		    	extraData();
			});
		};

		// Find existing Proposal
		$scope.findOne = function() {
			Proposals.get({ 
				proposalId: $stateParams.proposalId
			}, function(proposal){
				$scope.proposal = proposal;
				console.log($scope.proposal);
				$scope.loadCitiesByProvince($scope.proposal.province.id);
			});
			$scope.loadProvinces();
		};

		$scope.CreateEvent = function(proposalId) {
        	$location.path('proposals/'+proposalId+'/events/create')
    	};

    	$scope.CreateColaboration = function() {
        	$location.path('proposals/'+$scope.proposal.id+'/colaborations/create')
    	};

    	// Inyect provinces
		$scope.loadProvinces = function() {
			$scope.provinces = Provinces.query();
		};

		// Inyect cities
		$scope.loadCitiesByProvince = function(provinceId) {
            if($scope.province != ""){
	            Cities.byProvince({
		            provinceId: provinceId
		        }, function(cities) {
		            $scope.cities = cities;
		        });
	        }
        };

        $scope.loadArtists = function() {
        	$http.get('/getPublicArtists').success(function(data){
        		$scope.artists = data;
        	});
        };

        $scope.loadShowSites = function(){
        	$http.get('/showSites').success(function(data){
        		$scope.showSites = data;
        	});
        }

		$scope.preCreate = function(){
			$scope.loadArtists();
			$scope.loadProvinces();
			$scope.loadShowSites();
			$scope.typeSS = "list";
		};

		$scope.approvedEvent = function(proposal){
			$http.get('/colaborations/getTotal/'+proposal.id).success(function(data) {
            	$scope.total = data.total;
            	console.log(data);
                return $scope.total >= proposal.colaborationsQuantity;
            });
		};

		$scope.cancelProposal = function() {
        	var proposal = $scope.proposal ;
        	proposal.state = "Cancelado";
			proposal.$update(function() {
				$location.path('proposals/' + proposal.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
    	};

		$scope.findUser = function(id){
			console.log(id);
        	$http.get('/getUser/'+id).success(function(data) {
                $scope.foundUser = data;
            });
        };

        $scope.findProvince = function(id){
        	$http.get('/provinces/'+id).success(function(data) {
                $scope.foundProvince = data;
            });
        };

        $scope.findCity = function(id){
        	$http.get('/cities/'+id).success(function(data) {
                $scope.foundCity = data;
            });
        };

        $scope.byAgent = function() {
        	$http.get('/proposals/byagent').success(function(data){
        		$scope.proposals = data;
        		extraData();
        	});
        };

        $scope.byArtist = function() {
        	$http.get('/myProposalsArtist').success(function(data){
        		$scope.proposals = data;
        	});
        };

        $scope.findPublic = function() {
        	$http.get('/proposalsPublics').success(function(data){
        		$scope.proposals = data;
        	});
        };

        $scope.acceptProposal = function() {
        	var proposal = $scope.proposal ;
        	proposal.state = "Revision";
			proposal.$update(function() {
				$location.path('proposals/' + proposal.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
    	};

	}
]);

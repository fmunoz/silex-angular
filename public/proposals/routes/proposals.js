'use strict';

//Setting up route
angular.module('mean.proposals').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Proposals state routing
		$stateProvider.
		state('listProposalsByAgent', {
            url: '/proposals/byAgent',
            templateUrl: 'public/proposals/views/listByAgent.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('listProposalsByArtist', {
            url: '/proposalsByArtist',
            templateUrl: 'public/proposals/views/listByArtist.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('listProposals', {
			url: '/proposals',
			templateUrl: 'public/proposals/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
        state('listProposalsPublics', {
            url: '/proposalsPublics',
            templateUrl: 'public/proposals/views/listPublic.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
		state('createProposal', {
			url: '/proposals/create',
			templateUrl: 'public/proposals/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewProposal', {
			url: '/proposals/:proposalId',
			templateUrl: 'public/proposals/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editProposal', {
			url: '/proposals/:proposalId/edit',
			templateUrl: 'public/proposals/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
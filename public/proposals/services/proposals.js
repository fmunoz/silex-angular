'use strict';

//Proposals service used to communicate Proposals REST endpoints
angular.module('mean.proposals').factory('Proposals', ['$resource',
	function($resource) {
		return $resource('proposals/:proposalId', { proposalId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

angular.module('mean.provinces').controller('ProvincesController', ['$scope', '$stateParams', '$location', 'Global', 'Provinces', function ($scope, $stateParams, $location, Global, Provinces) {
    $scope.global = Global;

    $scope.create = function() {
        var province = new Provinces({
            name: this.name
        });
        province.$save(function(response) {
            $location.path('provinces/' + response.id);
        });

        this.name = '';
    };

    $scope.remove = function(province) {
        if (province) {
            province.$remove();

            for (var i in $scope.provinces) {
                if ($scope.provinces[i] === province) {
                    $scope.provinces.splice(i, 1);
                }
            }
        }
        else {
            $scope.province.$remove();
            $location.path('provinces');
        }
    };

    $scope.update = function() {
        var province = $scope.province;
        province.$update(function() {
            $location.path('provinces/' + province.id);
        });
    };

    $scope.find = function() {
        Provinces.query(function(provinces) {
            $scope.provinces = provinces;
        });
    };

    $scope.findOne = function() {
        Provinces.get({
            provinceId: $stateParams.provinceId
        }, function(province) {
            $scope.province = province;
        });
    };

    $scope.addCity = function() {
        $location.path('provinces/'+$scope.province.id+'/cities/create')
    }

}]);
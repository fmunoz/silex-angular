'use strict';

//Setting up route
angular.module('mean.provinces').config(['$stateProvider',
    function($stateProvider) {
        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('listProvinces', {
                url: '/provinces',
                templateUrl: 'public/provinces/views/list.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('createProvince', {
                url: '/provinces/create',
                templateUrl: 'public/provinces/views/create.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('edit province', {
                url: '/provinces/:provinceId/edit',
                templateUrl: 'public/provinces/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('province by id', {
                url: '/provinces/:provinceId',
                templateUrl: 'public/provinces/views/view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);

'use strict';

//Provinces service used for provinces REST endpoint
angular.module('mean.provinces').factory('Provinces', ['$resource', function($resource) {
    return $resource('provinces/:provinceId', {
        provinceId: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);
'use strict';

// Releases controller
angular.module('mean.releases').controller('ReleasesController', ['$scope', '$stateParams', '$location', 'Global', 'Releases',
	function($scope, $stateParams, $location, Global, Releases ) {
		$scope.global = Global;

		// Create new Release
		$scope.create = function() {
			// Create new Release object
			var release = new Releases ({
				date: "now",
				title: this.title,
				content: this.content
			});

			// Redirect after save
			release.$save(function(response) {
				$location.path('releases/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
		};

		// Remove existing Release
		$scope.remove = function( release ) {
			if ( release ) { release.$remove();

				for (var i in $scope.releases ) {
					if ($scope.releases [i] === release ) {
						$scope.releases.splice(i, 1);
					}
				}
			} else {
				$scope.release.$remove(function() {
					$location.path('releases');
				});
			}
		};

		// Update existing Release
		$scope.update = function() {
			var release = $scope.release ;

			release.$update(function() {
				$location.path('releases/' + release.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Releases
		$scope.find = function() {
			$scope.releases = Releases.query();
		};

		// Find existing Release
		$scope.findOne = function() {
			$scope.release = Releases.get({ 
				releaseId: $stateParams.releaseId
			});
		};
	}
]);
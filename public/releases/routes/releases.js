'use strict';

//Setting up route
angular.module('mean.releases').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Releases state routing
		$stateProvider.
		state('listReleases', {
			url: '/releases',
			templateUrl: 'public/releases/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createRelease', {
			url: '/releases/create',
			templateUrl: 'public/releases/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewRelease', {
			url: '/releases/:releaseId',
			templateUrl: 'public/releases/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editRelease', {
			url: '/releases/:releaseId/edit',
			templateUrl: 'public/releases/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
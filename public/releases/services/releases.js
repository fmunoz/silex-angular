'use strict';

//Releases service used to communicate Releases REST endpoints
angular.module('mean.releases').factory('Releases', ['$resource',
	function($resource) {
		return $resource('releases/:releaseId', { releaseId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Showsites controller
angular.module('mean.showsites').controller('ShowsitesController', ['$scope', '$stateParams', '$location', 'Global', 'Showsites', 'Provinces', 'Cities', '$http',
	function($scope, $stateParams, $location, Global, Showsites, Provinces, Cities, $http ) {
		$scope.global = Global;

		// Create new Showsite
		$scope.create = function() {
			// Create new Showsite object
			var showsite = new Showsites ({
				name: this.name,
				addres: this.addres,
				capacity : this.capacity,
				province : this.province,
				city : this.city
			});

			// Redirect after save
			showsite.$save(function(response) {
				$location.path('showSites/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
			this.addres = '';
			this.capacity = '';
		};

		// Remove existing Showsite
		$scope.remove = function( showsite ) {
			if ( showsite ) { showsite.$remove();

				for (var i in $scope.showsites ) {
					if ($scope.showsites [i] === showsite ) {
						$scope.showsites.splice(i, 1);
					}
				}
			} else {
				$scope.showsite.$remove(function() {
					$location.path('showSites');
				});
			}
		};

		// Update existing Showsite
		$scope.update = function() {
			var showsite = new Showsites ({
				id: $scope.showsite.id,
				name: $scope.showsite.name,
				addres: $scope.showsite.addres,
				capacity : $scope.showsite.capacity,
				province : $scope.showsite.province,
				city : $scope.showsite.city
			});
			$scope.showsite = showsite;

			showsite.$update(function() {
				$location.path('showSitesEdited');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Showsites
		$scope.find = function() {
			$scope.showsites = Showsites.query();
		};

		// Find existing Showsite
		$scope.findOne = function() {
			$scope.showsite = Showsites.get({ 
				showsiteId: $stateParams.showSiteId
			});
		};

		// Find existing Showsite Edited
		$scope.findOneEdited = function() {
			$scope.showsiteEdited = $scope.showsite;
			console.log($scope.showsite);
		};

		$scope.getShowSiteByUser = function(){
        	$http.get('/showSitesByUser').success(function(data) {
                $scope.showsite = data;
            });
            $scope.loadProvinces();
        };

        $scope.findUser = function(id){
			console.log(id);
        	$http.get('/getUser/'+id).success(function(data) {
                $scope.foundUser = data;
            });
        };

        $scope.findProvince = function(id){
        	$http.get('/provinces/'+id).success(function(data) {
                $scope.foundProvince = data;
            });
        };

        $scope.findCity = function(id){
        	$http.get('/cities/'+id).success(function(data) {
                $scope.foundCity = data;
            });
        };

        $scope.sendMessage = function(userId) {
            $location.path('messages/create/'+userId)
        };

        $scope.findPublic = function() {
	        $http.get('/showSitesPublics').success(function(data) {
	                $scope.showsites = data;
         	});
	    };

	    // Inyect provinces
		$scope.loadProvinces = function() {
			$scope.provinces = Provinces.query();
		};

		// Inyect cities
		$scope.loadCitiesByProvince = function(provinceId) {
            if($scope.province != ""){
	            Cities.byProvince({
		            provinceId: provinceId
		        }, function(cities) {
		            $scope.cities = cities;
		        });
	        }
        };

	}
]);
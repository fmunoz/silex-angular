'use strict';

//Setting up route
angular.module('mean.showsites').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Showsites state routing
		$stateProvider.
		state('listShowSites', {
			url: '/showSites',
			templateUrl: 'public/showsites/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
        state('listShowSitesPublics', {
            url: '/showSitesPublics',
            templateUrl: 'public/showsites/views/listPublic.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('editShowsite', {
            url: '/showSites/edit',
            templateUrl: 'public/showsites/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
		state('createShowSite', {
			url: '/showSites/create',
			templateUrl: 'public/showsites/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewShowSite', {
			url: '/showSites/:showSiteId',
			templateUrl: 'public/showsites/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
        state('viewShowSiteEdited', {
            url: '/showSitesEdited',
            templateUrl: 'public/showsites/views/viewEdited.html',
            resolve: {
                loggedin: checkLoggedin
            }
        });
	}
]);
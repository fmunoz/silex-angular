'use strict';

//Showsites service used to communicate Showsites REST endpoints
angular.module('mean.showsites').factory('Showsites', ['$resource',
	function($resource) {
		return $resource('showSites/:showsiteId', { showsiteId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Tickets controller
angular.module('mean.tickets').controller('TicketsController', ['$scope', '$stateParams', '$location', 'Global', 'Tickets',
	function($scope, $stateParams, $location, Global, Tickets ) {
		$scope.global = Global;

		// Create new Ticket
		$scope.create = function(price,description,event) {
			// Create new Ticket object
			var ticket = new Tickets ({
				price: price,
				description: description,
				event: event.id
			});

			// Redirect after save
			ticket.$save(function(response) {
				$location.path('tickets/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Remove existing Ticket
		$scope.remove = function( ticket ) {
			if ( ticket ) { ticket.$remove();

				for (var i in $scope.tickets ) {
					if ($scope.tickets [i] === ticket ) {
						$scope.tickets.splice(i, 1);
					}
				}
			} else {
				$scope.ticket.$remove(function() {
					$location.path('tickets');
				});
			}
		};

		// Update existing Ticket
		$scope.update = function() {
			var ticket = $scope.ticket ;

			ticket.$update(function() {
				$location.path('tickets/' + ticket.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Tickets
		$scope.find = function() {
			$scope.tickets = Tickets.query();
		};

		// Find existing Ticket
		$scope.findOne = function() {
			$scope.ticket = Tickets.get({ 
				ticketId: $stateParams.ticketId
			});
		};
	}
]);
'use strict';

//Setting up route
angular.module('mean.tickets').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Tickets state routing
		$stateProvider.
		state('listTickets', {
			url: '/tickets',
			templateUrl: 'public/tickets/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createTicket', {
			url: '/promos/:promoId/tickets/create',
			templateUrl: 'public/tickets/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewTicket', {
			url: '/tickets/:ticketId',
			templateUrl: 'public/tickets/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editTicket', {
			url: '/tickets/:ticketId/edit',
			templateUrl: 'public/tickets/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
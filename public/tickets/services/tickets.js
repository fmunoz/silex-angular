'use strict';

//Tickets service used to communicate Tickets REST endpoints
angular.module('mean.tickets').factory('Tickets', ['$resource',
	function($resource) {
		return $resource('tickets/:ticketId', { ticketId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Users controller
angular.module('mean.users').controller('UsersController', ['$scope', '$stateParams', '$location', 'Global', 'Users', '$http',
	function($scope, $stateParams, $location, Global, Users, $http ) {
		$scope.global = Global;

		// Create new User
		$scope.create = function() {
			// Create new User object
			var user = new Users ({
				name: this.name
			});

			// Redirect after save
			user.$save(function(response) {
				$location.path('users/' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
		};

		// Remove existing User
		$scope.remove = function( user ) {
			if ( user ) { user.$remove();

				for (var i in $scope.users ) {
					if ($scope.users [i] === user ) {
						$scope.users.splice(i, 1);
					}
				}
			} else {
				$scope.user.$remove(function() {
					$location.path('users');
				});
			}
		};

		// Update existing User
		$scope.update = function() {
			var user = $scope.user ;

			user.$update(function() {
				$location.path('users/' + user.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Update state of User 
		$scope.updateState = function(user,state) {
			user.state = state;

			user.$update(function() {
				$location.path('users/' + user.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Users
		$scope.find = function() {
			$scope.users = Users.query();
		};

		// Find existing User
		$scope.findOne = function() {
			$scope.user = Users.get({ 
				userId: $stateParams.userId
			});
		};



		// Register the login() function
        $scope.login = function(){
            $http.post('/login', {
                email: $scope.user.email,
                password: $scope.user.password
            })
                .success(function(user){
                    // authentication OK
                    $scope.loginError = 0;
                    $rootScope.user = user;
                    $rootScope.$emit('loggedin');
                    $location.url('/');
                })
                .error(function() {
                    $scope.loginerror = 'Authentication failed.';
                });
        };
        // Register the login() function
        $scope.logout = function(){
            $http.get('/logout', {
            })
                .success(function(data){
                    // logout OK
                    $rootScope.user = null;
                    $rootScope.$emit('loggedin');
                    $location.url('/');

                })
                .error(function() {
                    $scope.logouterror = 'Logout failed.';
                });
        };

 
        $scope.register = function(){
            $scope.usernameError = null;
            $scope.registerError = null;
            $http.post('/register', {
                email: $scope.user.email,
                password: $scope.user.password,
                confirmPassword: $scope.user.confirmPassword,
                username: $scope.user.username,
                name: $scope.user.name
            })
                .success(function(){
                    // authentication OK
                    $scope.registerError = 0;
                    $rootScope.user = $scope.user;
                    $rootScope.$emit('loggedin');
                    $location.url('/');
                })
                .error(function(error){
                    // Error: authentication failed
                    if (error === 'Username already taken') {
                        $scope.usernameError = error;
                    }
                    else {
                        $scope.registerError = error;
                    }
                });
        };

        $scope.getFans = function(){
            $http.get('/getFans').success(function(data) {
                $scope.fans = data;
            });
        };

        $scope.getAgents = function(){
            $http.get('/getAgents').success(function(data) {
                $scope.agents = data;
            });
        };

        $scope.getShowSites = function(){
            $http.get('/showSites').success(function(data) {
                $scope.showSites = data;
            });
        };

        $scope.getArtists = function(){
            $http.get('/getArtists').success(function(data) {
                $scope.artists = data;
            });
        };

        $scope.getPublicArtists = function(){
            $http.get('/getPublicArtists').success(function(data) {
                $scope.artists = data;
            });
        };

        $scope.sendMessage = function(userId) {
            $location.path('messages/create/'+userId)
        };



	}
]);
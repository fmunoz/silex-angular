'use strict';

//Setting up route
angular.module('mean.users').config(['$stateProvider',
	function($stateProvider) {

        var checkLoggedOut = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }

                // Not Authenticated
                else $timeout(deferred.resolve);
            });

            return deferred.promise;
        };

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Users state routing
		$stateProvider.
		state('listUsers', {
			url: '/users',
			templateUrl: 'public/users/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createUser', {
			url: '/users/create',
			templateUrl: 'public/users/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewUser', {
			url: '/users/:userId',
			templateUrl: 'public/users/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
        state('fansList', {
            url: '/fansList',
            templateUrl: 'public/users/views/fansList.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('showSitesList', {
            url: '/showSitesList',
            templateUrl: 'public/users/views/showSitesList.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('artistPublicsList', {
            url: '/artistsPublics',
            templateUrl: 'public/users/views/artistsList.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
        state('agentsList', {
            url: '/agentsList',
            templateUrl: 'public/users/views/agentsList.html',
            resolve: {
                loggedin: checkLoggedin
            }
        }).
		state('editUser', {
			url: '/users/:userId/edit',
			templateUrl: 'public/users/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});

	}
]);

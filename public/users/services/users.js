'use strict';

//Users service used to communicate Users REST endpoints
angular.module('mean.users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users/:userId', { userId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Votes controller
angular.module('mean.votes').controller('VotesController', ['$scope', '$stateParams', '$http','$location', 'Global', 'Votes','Users','Provinces','Cities',
	function($scope, $stateParams, $http,$location, Global, Votes, Users, Provinces, Cities) {
		$scope.global = Global;

		// Create new Vote
		$scope.create = function() {
			// Create new Vote object
			var vote = new Votes ({
				province : this.province,
				city : this.city,
				artist : this.artist
			});

			// Redirect after save
			vote.$save(function(response) {
				$location.path('' + response.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Remove existing Vote
		$scope.remove = function( vote ) {
			if ( vote ) { vote.$remove();

				for (var i in $scope.votes ) {
					if ($scope.votes [i] === vote ) {
						$scope.votes.splice(i, 1);
					}
				}
			} else {
				$scope.vote.$remove(function() {
					$location.path('votes');
				});
			}
		};

		// Update existing Vote
		$scope.update = function() {
			var vote = $scope.vote ;

			vote.$update(function() {
				$location.path('votes/' + vote.id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Votes
		$scope.find = function() {
			$scope.votes = Votes.query();
		};

		// Find existing Vote
		$scope.findOne = function() {
			$scope.vote = Votes.get({ 
				voteId: $stateParams.voteId
			});
		};

		$scope.getArtists = function() {
			$http.get('/getArtists').success(function(artists){
				$scope.artists = artists;
			});
		};

		// Inyect provinces
		$scope.loadProvinces = function() {
			$scope.provinces = Provinces.query();
		};

		// Inyect cities
		$scope.loadCitiesByProvince = function(provinceId) {
            if($scope.province != ""){
	            Cities.byProvince({
		            provinceId: provinceId
		        }, function(cities) {
		            $scope.cities = cities;
		        });
	        }
        };

		$scope.preCreate = function(){
			$scope.getArtists();
			$scope.loadProvinces();
		};

		$scope.getVotesByArtists = function() {
			$http.get('/votesbyartist').success(function(votesByArtists){
				$scope.votesByArtists = votesByArtists;
			});
		};
	}
]);
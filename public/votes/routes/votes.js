'use strict';

//Setting up route
angular.module('mean.votes').config(['$stateProvider',
	function($stateProvider) {

        // Check if the user is connected
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') $timeout(deferred.resolve);

                // Not Authenticated
                else {
                    $timeout(deferred.reject);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

		// Votes state routing
		$stateProvider.
		state('listVotes', {
			url: '/votes',
			templateUrl: 'public/votes/views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('createVote', {
			url: '/votes/create',
			templateUrl: 'public/votes/views/create.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('viewVote', {
			url: '/votes/:voteId',
			templateUrl: 'public/votes/views/view.html',
            resolve: {
                loggedin: checkLoggedin
            }
		}).
		state('editVote', {
			url: '/votes/:voteId/edit',
			templateUrl: 'public/votes/views/edit.html',
            resolve: {
                loggedin: checkLoggedin
            }
		});
	}
]);
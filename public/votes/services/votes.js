'use strict';

//Votes service used to communicate Votes REST endpoints
angular.module('mean.votes').factory('Votes', ['$resource',
	function($resource) {
		return $resource('votes/:voteId', { voteId: '@id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
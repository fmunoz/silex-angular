<?php
putenv("SILEX_MODE=test");

$app = require('app/app.php');
$app["debug"] = true;

$user = $app["users.repository"]->findOneBy(array("email" => "hola@showme.com"));
$userArr = array("email" => "hola@showme.com", "password" => "1","rol" => "admin");

if($user == null) {
	$user = $app["users.entity"]->create($userArr);
	$user->persist();
    $app["db.orm.em"]->flush();
}
$userAdmin = $app["admins.repository"]->findOneBy(array("email" => "hola@showme.com"));
$userAdminArr = array("email" => "hola@showme.com", "password" => "1","rol" => "admin");

if($userAdmin == null) {
    $userAdmin = $app["admins.entity"]->create($userAdminArr);
    $userAdmin->persist();
    $app["db.orm.em"]->flush();
}




trait LoginTrait {
	protected function login() {
		global $userArr;

        $headers = array("Content-Type" => "application/json");        
        $client = $this->createClient();
        //loggin del usuario
        $client->request("POST", "/login", array(), array(), $headers, json_encode($userArr));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
	}

	protected function logout() {
        $client = $this->createClient();
        $client->request("GET", "/logout", array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
	}

    protected function loginAdmin() {
        global $userAdminArr;

        $headers = array("Content-Type" => "application/json");        
        $client = $this->createClient();
        //loggin del usuario
        $client->request("POST", "/loginAdmin", array(), array(), $headers, json_encode($userAdminArr));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
    }

    protected function logoutAdmin() {
        $client = $this->createClient();
        $client->request("GET", "/logoutAdmin", array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
    }
}

?>
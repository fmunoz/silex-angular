<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class AdminsControllerTest extends WebTestCase {


    public function createApplication()
    {
        global $app;
        return $app;
    }


    function setUp() {
        parent::setUp();
    }

    function tearDown() {
        parent::tearDown();
    }


    function userAdminProvider() {
        return array(
            array(
                array("email"    => "prueba4@prueba.com", 
                      "id" => "null",
                      "password" => "prueba",
                      "dni" => "abc",
                      "name" => "abc",
                      "surname" => "abc",
                      "phone" => "abc",
                    ),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider userAdminProvider
     */
    public function testCreate($user, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/registerAdmin", array(), array(), $headers, json_encode($user));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["email"], "prueba4@prueba.com");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testCreate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/usersAdmin/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }

}

?>
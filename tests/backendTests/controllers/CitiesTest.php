<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class CitiesControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->loginAdmin();

    } 

    function tearDown() {
        parent::tearDown();
        $this->logoutAdmin();
    }


    function cityProvider() {
        return array(
            array(
                array("name"    => "cityOr"),
                array("name"    => "provinceName"),
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function cityUpdateProvider() {
        return array(
            array(
                array("name"    => "cityMod"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider cityProvider
     */
    public function testCreate($city, $province, $headers) {
        $client = $this->createClient();
        //creacion de la provincia
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "provinceName");    

        $GLOBALS["idProvinceRemove"] = $data["id"];

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvinceRemove"];
        
        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "cityOr");    
        $GLOBALS["idCityRemove"] = $data["id"];
        
    }

     /**
     * @depends testCreate
     * @dataProvider cityUpdateProvider
     */
    public function testUpdate($city, $headers) {
        $client = $this->createClient();
        $idCityRemove = $GLOBALS["idCityRemove"];
        $this->assertTrue($GLOBALS["idCityRemove"] != '');  
        $this->assertTrue($GLOBALS["idCityRemove"] != null);  
        $client->request("PUT", "/cities/".$idCityRemove, array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "cityMod");    
        $this->assertTrue($GLOBALS["idCityRemove"] == $data["id"]);  
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["idCityRemove"];
        $client->request("DELETE", "/cities/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   

        $removeId = $GLOBALS["idProvinceRemove"];
        $client->request("DELETE", "/provinces/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   
    }

   


}

?>
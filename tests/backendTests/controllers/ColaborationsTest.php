<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class ColaborationsControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->loginAdmin();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logoutAdmin();
    }

    function providerFromColaboration() {
        return array(
                array(
                    array(
                        "dateInit"                  => "now",
                        "deadLine"                  => "now",
                        "colaborationsQuantity"     => "10",
                        "picture"                   => "path",
                        "showSiteName"              => "site_name",
                        "showSiteAddres"            => "site_addres",
                        "state"                     => "active"
                        ),
                    array(
                        "mount" => "100",
                        "date"  => "now"
                        ),
                    array("Content-Type" => "application/json"),
                    array("name" => "cordoba"),
                ),
        );
    }

    function updateProviderFromColaboration() {
        return array(
            array(
                array("mount" => "101"),
                array("Content-Type" => "application/json")
            ),
        );
    }

    /**
     * @dataProvider providerFromColaboration
     */
    public function testCreate($proposal,$colaboration,$headers,$province) {
        $client = $this->createClient();    
        
        //creacion de la provincia
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "cordoba");    

        $GLOBALS["idProvinceColaboration"] = $data["id"];

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvinceColaboration"];
        $city["name"] = "rio4";

        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "rio4");    
        $GLOBALS["idCityColaboration"] = $data["id"];
        
        $this->logoutAdmin();
        $this->login();
        // creacion de showSite
        $showSite = array("name" => "nombre","addres" => "constutucion","capacity"  => "240");
        
        $client->request("POST", "/showSites", array(), array(), $headers, json_encode($showSite));
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);
        $this->assertTrue($response->isOk());
        $GLOBALS["idShowSiteColaboration"] = $data["id"];

        //agrego al array province, city y showSite
        $proposal["province"] = $GLOBALS["idProvinceColaboration"];
        $proposal["showSite"] = $GLOBALS["idShowSiteColaboration"];
        $proposal["city"] = $GLOBALS["idCityColaboration"];

        //creacion de la propuesta
        $client->request("POST", "/proposals", array(), array(), $headers, json_encode($proposal));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $GLOBALS["idProposalColaboration"] = $data["id"];        

        //creacion de la colaboracion
        $colaboration["proposal"] = $GLOBALS["idProposalColaboration"];
        $colaboration["user"] = $GLOBALS["user"]->getId();
        
        $client->request("POST", "/colaborations", array(), array(), $headers, json_encode($colaboration));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $GLOBALS["removeIdColaboration"] = $data["id"];

        $this->logout();
        $this->loginAdmin();
    }

     /**
     * @depends testCreate
     * @dataProvider updateProviderFromColaboration
     */
    public function testUpdate($colaboration, $headers) {
        $client = $this->createClient();

        $this->assertTrue($GLOBALS["removeIdColaboration"] != '');  
        $this->assertTrue($GLOBALS["removeIdColaboration"] != null);  
        $client->request("PUT", "/colaborations/".$GLOBALS["removeIdColaboration"], array(), array(), $headers, json_encode($colaboration));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($data["mount"], "101");
        $this->assertEquals($data["id"], $GLOBALS["removeIdColaboration"]);
    }   

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $headers = array("Content-Type" => "application/json");
        $client = $this->createClient();
        
        //eliminar colaboration
        $client->request("DELETE", "/colaborations/".$GLOBALS["removeIdColaboration"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        //eliminar proposal
        $client->request("DELETE", "/proposals/".$GLOBALS["idProposalColaboration"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        // eliminar ciudad
        $client->request("DELETE", "/cities/".$GLOBALS["idCityColaboration"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar provincia
        $client->request("DELETE", "/provinces/".$GLOBALS["idProvinceColaboration"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar showSite
        $client->request("DELETE", "/showSites/".$GLOBALS["idShowSiteColaboration"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
         
    }

}

?>
<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class EventsControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function providerFromEvent() {
        return array(
                array(
                    array(
                        "dateInit"                  => "now",
                        "deadLine"                  => "now",
                        "colaborationsQuantity"     => "101",
                        "picture"                   => "path",
                        "showSiteName"              => "site_name",
                        "showSiteAddres"            => "site_addres",
                        "state"                     => "active",
                        "ticketPrice"               => "1500",
                        "ticketsAvailablesQuantity" => "40",
                        "ticketsSoldsQuantity"      => "50"
                        ),
                    array("Content-Type" => "application/json"),
                    array("name" => "cordoba"),
                ),
        );
    }

    function updateProviderFromEvent() {
        return array(
            array(
                array(
                    "colaborationsQuantity"     => "11",
                    "picture"                   => "path_new",
                    "state"                     => "terminated",
                    "ticketPrice"               => "3000",
                    "ticketsAvailablesQuantity" => "100",
                    "ticketsSoldsQuantity"      => "150"
                    ),
                array("Content-Type" => "application/json")
            ),
        );
    }

    /**
     * @dataProvider providerFromEvent
     */
    public function testCreate($event, $headers,$province) {
        $client = $this->createClient();    
        
        //creacion de la provincia
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "cordoba");    

        $GLOBALS["idProvinceEvent"] = $data["id"];

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvinceEvent"];
        $city["name"] = "rio4";

        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "rio4");    
        $GLOBALS["idCityEvent"] = $data["id"];

        // creacion de showSite
        $showSite = array("name" => "nombre" ,"addres"    => "constutucion","capacity"  => "240");
        
        $client->request("POST", "/showSites", array(), array(), $headers, json_encode($showSite));
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);
        $this->assertTrue($response->isOk());
        $GLOBALS["idShowSiteEvent"] = $data["id"];

        //agrego al array province, city y showSite
        $event["province"] = $GLOBALS["idProvinceEvent"];
        $event["showSite"] = $GLOBALS["idShowSiteEvent"];
        $event["city"] = $GLOBALS["idCityEvent"];
        

        //creacion de la propuesta
        $client->request("POST", "/events", array(), array(), $headers, json_encode($event));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["city"]["id"], $GLOBALS["idCityEvent"]); 
        $GLOBALS["removeIdEvent"] = $data["id"];

    }

     /**
     * @depends testCreate
     * @dataProvider updateProviderFromEvent
     */
    public function testUpdate($event, $headers) {
        $client = $this->createClient();

        $this->assertTrue($GLOBALS["removeIdEvent"] != '');  
        $this->assertTrue($GLOBALS["removeIdEvent"] != null);  
        $client->request("PUT", "/events/".$GLOBALS["removeIdEvent"], array(), array(), $headers, json_encode($event));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($data["colaborationsQuantity"], "11");    
        $this->assertEquals($data["picture"], "path_new");
        $this->assertEquals($data["state"], "terminated");
        $this->assertEquals($data["ticketPrice"], "3000");
        $this->assertEquals($data["ticketsAvailablesQuantity"], "100");
        $this->assertEquals($data["ticketsSoldsQuantity"], "150");
        $this->assertEquals($data["id"], $GLOBALS["removeIdEvent"]);
    }   

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $headers = array("Content-Type" => "application/json");
        $client = $this->createClient();
        
        //eliminar event
        $client->request("DELETE", "/events/".$GLOBALS["removeIdEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        // eliminar ciudad
        $client->request("DELETE", "/cities/".$GLOBALS["idCityEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar provincia
        $client->request("DELETE", "/provinces/".$GLOBALS["idProvinceEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar showSite
        $client->request("DELETE", "/showSites/".$GLOBALS["idShowSiteEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
         
    }

}

?>
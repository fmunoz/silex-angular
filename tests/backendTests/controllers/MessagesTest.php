<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class MessagesControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function messagesProvider() {
        return array(
            array(
                array("subject"    => "subjectOriginal",
                    "content"    => "contentOriginal",
                    "attachment"    => "attachmentOriginal",
                    "date"    => "now",
                    "state"    => "stateOriginal"),      
                array("email" => "chau@showme.com",
                        "rol" => "admin",
                    "password"  => "1"),
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function messagesUpdateProvider() {
        return array(
            array(
                array("date"    => "now",
                    "state"    => "stateActualizado"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider messagesProvider
     */
    public function testCreate($message, $destinatary, $headers) {
        $this->logout();
        $client = $this->createClient();

        //creacion del usuario destinatario
        $client->request("POST", "/register", array(), array(), $headers, json_encode($destinatary));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["email"], "chau@showme.com");    
        $GLOBALS["idDestinataryRemove"] = $data["id"];

        $this->login();
        //creacion del mensaje
        $message["userReceiver"] = $GLOBALS["idDestinataryRemove"];
        $client->request("POST", "/messages", array(), array(), $headers, json_encode($message));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["subject"], "subjectOriginal"); 
        $this->assertEquals($data["content"], "contentOriginal"); 
        $this->assertEquals($data["attachment"], "attachmentOriginal"); 
        $this->assertEquals($data["state"], "stateOriginal"); 
        
        $GLOBALS["idMessageRemove"] = $data["id"];
                
    }   


    /**
     * @depends testCreate
     * @dataProvider messagesUpdateProvider
     */
    public function testUpdate($message, $headers) {
        $client = $this->createClient();
        $idMessageRemove = $GLOBALS["idMessageRemove"];
        $this->assertTrue($GLOBALS["idMessageRemove"] != '');  
        $this->assertTrue($GLOBALS["idMessageRemove"] != null);  
        $client->request("PUT", "/messages/".$idMessageRemove, array(), array(), $headers, json_encode($message));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["state"], "stateActualizado");    
        $this->assertTrue($GLOBALS["idMessageRemove"] == $data["id"]);
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["idMessageRemove"];
        $client->request("DELETE", "/messages/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $removeId = $GLOBALS["idDestinataryRemove"];
        $client->request("DELETE", "/users/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   
    }

   
}

?>
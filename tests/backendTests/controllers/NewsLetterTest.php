<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class NewsLetterControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->loginAdmin();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logoutAdmin();
    }

    function newsLetterProvider() {
        return array(
            array(
                array("date"    => "now",
                      "subject" => "AsuntoOrig",
                      "content" => "ContentOrig"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function newsLetterUpdateProvider() {
        return array(
            array(
                array("subject"    => "AsuntoActualizado"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider newsLetterProvider
     */
    public function testCreate($newsLetter, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/newsLetters", array(), array(), $headers, json_encode($newsLetter));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["subject"], "AsuntoOrig"); 
        $this->assertEquals($data["content"], "ContentOrig"); 
        $GLOBALS["removeId"] = $data["id"];
    }

     /**
     * @depends testCreate
     * @dataProvider newsLetterUpdateProvider
     */
    public function testUpdate($newsLetter, $headers) {
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $this->assertTrue($GLOBALS["removeId"] != '');  
        $this->assertTrue($GLOBALS["removeId"] != null);  
        $client->request("PUT", "/newsLetters/".$removeId, array(), array(), $headers, json_encode($newsLetter));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["subject"], "AsuntoActualizado");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/newsLetters/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   
    }

}

?>
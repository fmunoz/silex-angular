<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class PhotosControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function photoProvider() {
        return array(
            array(
                array("path"    => "pathOriginal"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function photoUpdateProvider() {
        return array(
            array(
                array("path"    => "pathActualizado"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider photoProvider
     */
    public function testCreate($photo, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/photos", array(), array(), $headers, json_encode($photo));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["path"], "pathOriginal"); 
        $GLOBALS["removeId"] = $data["id"];
    }

     /**
     * @depends testCreate
     * @dataProvider photoUpdateProvider
     */
    public function testUpdate($photo, $headers) {
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $this->assertTrue($GLOBALS["removeId"] != '');  
        $this->assertTrue($GLOBALS["removeId"] != null);  
        $client->request("PUT", "/photos/".$removeId, array(), array(), $headers, json_encode($photo));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["path"], "pathActualizado");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/photos/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   
    }

}

?>
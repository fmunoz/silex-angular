<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class PromosControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->loginAdmin();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logoutAdmin();
    }

    function providerFromPromo() {
        return array(
                array(
                    array("name" => "cordoba"), //province
                    array( //event
                        "dateInit"                  => "now",
                        "deadLine"                  => "now",
                        "colaborationsQuantity"     => "101",
                        "picture"                   => "path",
                        "showSiteName"              => "site_name",
                        "showSiteAddres"            => "site_addres",
                        "state"                     => "active",
                        "ticketPrice"               => "1500",
                        "ticketsAvailablesQuantity" => "40",
                        "ticketsSoldsQuantity"      => "50"
                        ),
                    array( //proposal
                        "dateInit"                  => "now",
                        "deadLine"                  => "now",
                        "colaborationsQuantity"     => "10",
                        "picture"                   => "path",
                        "showSiteName"              => "site_name",
                        "showSiteAddres"            => "site_addres",
                        "state"                     => "active"
                        ),
                    array( //colaboration
                        "mount" => "100",
                        "date"  => "now"
                        ),
                    array( //promo
                        "discount"  => "100",
                        "decription"=> "descrip",
                        "price"     => "4000"
                        ),
                    array("Content-Type" => "application/json"),
                ),
        );
    }

    function updateProviderFromPromo() {
        return array(
            array(
                array( //promo
                        "discount"  => "150",
                        "decription"=> "descrip_update",
                        "price"     => "1200"
                        ),
                array("Content-Type" => "application/json")
            ),
        );
    }

    /**
     * @dataProvider providerFromPromo
     */
    public function testCreate($province,$event,$proposal,$colaboration,$promo,$headers) {
        $client = $this->createClient();    
        
        //creacion de la provincia
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "cordoba");    

        $GLOBALS["idProvincePromo"] = $data["id"];

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvincePromo"];
        $city["name"] = "rio4";

        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "rio4");    
        $GLOBALS["idCityPromo"] = $data["id"];

        // creacion de showSite
        $showSite = array("name" => "nombre" ,"addres"    => "constutucion","capacity"  => "240");
        
        $client->request("POST", "/showSites", array(), array(), $headers, json_encode($showSite));
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);
        $this->assertTrue($response->isOk());
        $GLOBALS["idShowSitePromo"] = $data["id"];

        //agrego a evento province, city y showSite
        $event["province"] = $GLOBALS["idProvincePromo"];
        $event["showSite"] = $GLOBALS["idShowSitePromo"];
        $event["city"] = $GLOBALS["idCityPromo"];
        

        //creacion de un evento
        $client->request("POST", "/events", array(), array(), $headers, json_encode($event));
        $response = $client->getResponse();
        
        $this->assertTrue($response->isOk());
        $data = json_decode($response->getContent(), true);
        $GLOBALS["idEventPromo"] = $data["id"];

        //agrego a propuesta: province, city y showSite
        $proposal["province"] = $GLOBALS["idProvincePromo"];
        $proposal["showSite"] = $GLOBALS["idShowSitePromo"];
        $proposal["city"] = $GLOBALS["idCityPromo"];

        //creacion de la propuesta
        $client->request("POST", "/proposals", array(), array(), $headers, json_encode($proposal));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $GLOBALS["idProposalPromo"] = $data["id"];

        //agrego a colaboracion propuesta
        $colaboration["proposal"] = $GLOBALS["idProposalColaboration"];
        $colaboration["user"] = $GLOBALS["user"]->getId();
        
        //creacion de colaboracion        
        $client->request("POST", "/colaborations", array(), array(), $headers, json_encode($colaboration));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $GLOBALS["idColaborationPromo"] = $data["id"];


        // agrego a promo colaboracion y evento
        $promo["colaboration"] = $GLOBALS["idColaborationPromo"];
        $promo["event"] = $GLOBALS["idEventPromo"];

        // creo la promo
        $client->request("POST", "/promos", array(), array(), $headers, json_encode($promo));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
        $data = json_decode($response->getContent(), true);
        $GLOBALS["removeIdPromo"] = $data["id"];
    }

     /**
     * @depends testCreate
     * @dataProvider updateProviderFromPromo
     */
    public function testUpdate($promo, $headers) {
        $client = $this->createClient();

        $this->assertTrue($GLOBALS["removeIdPromo"] != '');  
        $this->assertTrue($GLOBALS["removeIdPromo"] != null);  
        $client->request("PUT", "/promos/".$GLOBALS["removeIdPromo"], array(), array(), $headers, json_encode($promo));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($data["discount"], "150");    
        $this->assertEquals($data["decription"], "descrip_update");
        $this->assertEquals($data["price"], "1200");
        $this->assertEquals($data["id"], $GLOBALS["removeIdPromo"]);
    }   

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $headers = array("Content-Type" => "application/json");
        $client = $this->createClient();
        //eliminar promo
        $client->request("DELETE", "/promos/".$GLOBALS["removeIdPromo"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        //eliminar colaboracion
        $client->request("DELETE", "/colaborations/".$GLOBALS["idColaborationPromo"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        //eliminar propuesta
        $client->request("DELETE", "/proposals/".$GLOBALS["idProposalPromo"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        //eliminar event
        $client->request("DELETE", "/events/".$GLOBALS["removeIdEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar ciudad
        $client->request("DELETE", "/cities/".$GLOBALS["idCityEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar provincia
        $client->request("DELETE", "/provinces/".$GLOBALS["idProvinceEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar showSite
        $client->request("DELETE", "/showSites/".$GLOBALS["idShowSiteEvent"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
         
    }

}

?>
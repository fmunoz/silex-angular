<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class ProposalsControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function providerFromProposal() {
        return array(
                array(
                    array(
                        "dateInit"                  => "now",
                        "deadLine"                  => "now",
                        "colaborationsQuantity"     => "10",
                        "picture"                   => "path",
                        "showSiteName"              => "site_name",
                        "showSiteAddres"            => "site_addres",
                        "state"                     => "active"
                        ),
                    array("Content-Type" => "application/json"),
                    array("name" => "cordoba"),
                ),
        );
    }

    function updateProviderFromProposal() {
        return array(
            array(
                array(
                    "colaborationsQuantity" => "11",
                    "picture"               => "path_new",
                    "state"                 => "terminated"),
                array("Content-Type" => "application/json")
            ),
        );
    }

    /**
     * @dataProvider providerFromProposal
     */
    public function testCreate($proposal, $headers,$province) {
        $client = $this->createClient();    
        
        //creacion de la provincia
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "cordoba");    

        $GLOBALS["idProvinceProposal"] = $data["id"];

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvinceProposal"];
        $city["name"] = "rio4";

        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "rio4");    
        $GLOBALS["idCityProposal"] = $data["id"];

        // creacion de showSite
        $showSite = array("name" => "nombre","addres"    => "constutucion","capacity"  => "240");
        
        $client->request("POST", "/showSites", array(), array(), $headers, json_encode($showSite));
        $response = $client->getResponse();

        $data = json_decode($response->getContent(), true);
        $this->assertTrue($response->isOk());
        $GLOBALS["idShowSiteProposal"] = $data["id"];

        //agrego al array province, city y showSite
        $proposal["province"] = $GLOBALS["idProvinceProposal"];
        $proposal["showSite"] = $GLOBALS["idShowSiteProposal"];
        $proposal["city"] = $GLOBALS["idCityProposal"];

        //creacion de la propuesta
        $client->request("POST", "/proposals", array(), array(), $headers, json_encode($proposal));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["city"]["id"], $GLOBALS["idCityProposal"]); 
        $GLOBALS["removeIdProposal"] = $data["id"];

    }

     /**
     * @depends testCreate
     * @dataProvider updateProviderFromProposal
     */
    public function testUpdate($proposal, $headers) {
        $client = $this->createClient();

        $this->assertTrue($GLOBALS["removeIdProposal"] != '');  
        $this->assertTrue($GLOBALS["removeIdProposal"] != null);  
        $client->request("PUT", "/proposals/".$GLOBALS["removeIdProposal"], array(), array(), $headers, json_encode($proposal));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($data["colaborationsQuantity"], "11");    
        $this->assertEquals($data["picture"], "path_new");
        $this->assertEquals($data["state"], "terminated");
        $this->assertEquals($data["id"], $GLOBALS["removeIdProposal"]);
    }   

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $headers = array("Content-Type" => "application/json");
        $client = $this->createClient();
        
        //eliminar proposal
        $client->request("DELETE", "/proposals/".$GLOBALS["removeIdProposal"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        // eliminar ciudad
        $client->request("DELETE", "/cities/".$GLOBALS["idCityProposal"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar provincia
        $client->request("DELETE", "/provinces/".$GLOBALS["idProvinceProposal"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        // eliminar showSite
        $client->request("DELETE", "/showSites/".$GLOBALS["idShowSiteProposal"], array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
         
    }

}

?>
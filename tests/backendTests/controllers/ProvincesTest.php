<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class ProvincesControllerTest extends WebTestCase {


    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $user = array("email"    => "prueba@prueba.com", 
                        "rol" => "admin",
                      "password" => "prueba"
                    );
        $headers = array("Content-Type" => "application/json");
        
        $client = $this->createClient();
        //creacion del usuario
        $client->request("POST", "/register", array(), array(), $headers, json_encode($user));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);

        $GLOBALS["idUserRemove"] = $data["id"]; //salvamos la id para luego removerlo

        //loggin del usuario
        $client->request("POST", "/login", array(), array(), $headers, json_encode($user));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

    } 

    function tearDown() {
        parent::tearDown();

        $client = $this->createClient();
        //logout
        $client->request("GET", "/logout", array(), array(), array("Content-Type" => "application/json"), json_encode(array()));


        $removeId = $GLOBALS["idUserRemove"];
        $client->request("DELETE", "/users/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }


    function provinceProvider() {
        return array(
            array(
                array("name"    => "ProvinceName",
                      "id" => "null"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function provinceUpdateProvider() {
        return array(
            array(
                array("name"    => "ProvinceName2"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider provinceProvider
     */
    public function testCreate($province, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "ProvinceName");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
     * @depends testCreate
     * @dataProvider provinceUpdateProvider
     */
    public function testUpdate($province, $headers) {
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("PUT", "/provinces/".$removeId, array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "ProvinceName2");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/provinces/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }


}

?>
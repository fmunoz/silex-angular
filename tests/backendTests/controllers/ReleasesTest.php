<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class ReleasesControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->loginAdmin();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logoutAdmin();
    }

    function releaseProvider() {
        return array(
            array(
                array("date"    => "now",
                      "title" => "TituloOrig",
                      "content" => "ContentOrig"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function releaseUpdateProvider() {
        return array(
            array(
                array("title"    => "TituloActualizado"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider releaseProvider
     */
    public function testCreate($release, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/releases", array(), array(), $headers, json_encode($release));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["title"], "TituloOrig"); 
        $this->assertEquals($data["content"], "ContentOrig"); 
        $GLOBALS["removeId"] = $data["id"];
    }

     /**
     * @depends testCreate
     * @dataProvider releaseUpdateProvider
     */
    public function testUpdate($release, $headers) {
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $this->assertTrue($GLOBALS["removeId"] != '');  
        $this->assertTrue($GLOBALS["removeId"] != null);  
        $client->request("PUT", "/releases/".$removeId, array(), array(), $headers, json_encode($release));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["title"], "TituloActualizado");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/releases/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   
    }

}

?>
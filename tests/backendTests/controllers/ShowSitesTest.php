<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class ShowSitesControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function showSiteProvider() {
        return array(
            array(
                array("name" => "nombre",
                      "addres"    => "addres111",
                      "capacity"  => "100"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function showSiteUpdateProvider() {
        return array(
            array(
                array("addres"    => "addres222",
                      "name"      => "nombre",
                      "capacity"  => "220"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider showSiteProvider
     */
    public function testCreate($showSite, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/showSites", array(), array(), $headers, json_encode($showSite));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["addres"], "addres111"); 
        $this->assertEquals($data["capacity"], "100"); 
        $GLOBALS["idShowSiteRemove"] = $data["id"];
    }

     /**
     * @depends testCreate
     * @dataProvider showSiteUpdateProvider
     */
    public function testUpdate($showSite, $headers) {
        $client = $this->createClient();
        $idShowSiteRemove = $GLOBALS["idShowSiteRemove"];
        $this->assertTrue($GLOBALS["idShowSiteRemove"] != '');  
        $this->assertTrue($GLOBALS["idShowSiteRemove"] != null);  
        $client->request("PUT", "/showSites/".$idShowSiteRemove, array(), array(), $headers, json_encode($showSite));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["addres"], "addres222");    
        $this->assertEquals($data["capacity"], "220");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        /*$client = $this->createClient();
        $removeId = $GLOBALS["idShowSiteRemove"];
        $client->request("DELETE", "/showSites/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   */
    }

}

?>
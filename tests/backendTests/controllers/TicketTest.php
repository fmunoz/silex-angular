<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class TicketsControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function ticketsProvider() {
        return array(
            array(
                array("dateInit"    => "now",
                    "deadLine"    => "2014-08-25",
                    "colaborationsQuantity"    => "50",
                    "picture"    => "path",
                    "showSiteName"    => "nameShowSite",
                    "showSiteAddres"    => "addresShowSite",
                    "state"    => "inicial",
                    "ticketsAvailablesQuantity"    => "500",
                    "ticketsSoldsQuantity"    => "20",
                    "ticketPrice"    => "100"), 
                array("price" => "100",
                    "description" => "descripcionBreve"),
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function ticketsUpdateProvider() {
        return array(
            array(
                array("price"    => "150"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider ticketsProvider
     */
    public function testCreate($event, $ticket, $headers) {
        $client = $this->createClient();

        //creacion del evento
        $client->request("POST", "/events", array(), array(), $headers, json_encode($event));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["state"], "inicial");    
        $GLOBALS["idEventRemove"] = $data["id"];

        //creacion del ticket
        $ticket["event"] = $GLOBALS["idEventRemove"];
        $client->request("POST", "/tickets", array(), array(), $headers, json_encode($ticket));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
        
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["price"], "100"); 
        $this->assertEquals($data["description"], "descripcionBreve"); 
        
        $GLOBALS["idTicketRemove"] = $data["id"];
                
    }   


    /**
     * @depends testCreate
     * @dataProvider ticketsUpdateProvider
     */
    public function testUpdate($ticket, $headers) {
        $client = $this->createClient();
        $idTicketRemove = $GLOBALS["idTicketRemove"];
        $this->assertTrue($GLOBALS["idTicketRemove"] != '');  
        $this->assertTrue($GLOBALS["idTicketRemove"] != null);  
        $client->request("PUT", "/tickets/".$idTicketRemove, array(), array(), $headers, json_encode($ticket));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["price"], "150");    
        $this->assertTrue($GLOBALS["idTicketRemove"] == $data["id"]);
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["idTicketRemove"];
        $client->request("DELETE", "/tickets/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $removeId = $GLOBALS["idEventRemove"];
        $client->request("DELETE", "/events/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }

   
}

?>
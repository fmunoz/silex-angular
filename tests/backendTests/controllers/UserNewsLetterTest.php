<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class UserNewsLettersControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    function userNewsLetterProvider() {
        return array(
            array(
                array("email"    => "mail@mail.com"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    function userNewsLetterUpdateProvider() {
        return array(
            array(
                array("email"    => "nuevo@mail.com"),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider userNewsLetterProvider
     */
    public function testCreate($userNewsLetter, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/userNewsLetters", array(), array(), $headers, json_encode($userNewsLetter));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["email"], "mail@mail.com"); 
        $GLOBALS["removeId"] = $data["id"];
    }

     /**
     * @depends testCreate
     * @dataProvider userNewsLetterUpdateProvider
     */
    public function testUpdate($userNewsLetter, $headers) {
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $this->assertTrue($GLOBALS["removeId"] != '');  
        $this->assertTrue($GLOBALS["removeId"] != null);  
        $client->request("PUT", "/userNewsLetters/".$removeId, array(), array(), $headers, json_encode($userNewsLetter));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["email"], "nuevo@mail.com");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/userNewsLetters/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());   
    }

}

?>
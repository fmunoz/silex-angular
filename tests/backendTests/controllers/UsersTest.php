<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class UsersControllerTest extends WebTestCase {


    public function createApplication()
    {
        global $app;
        return $app;
    }


    function setUp() {
        parent::setUp();
    }

    function tearDown() {
        parent::tearDown();
    }


    function userProvider() {
        return array(
            array(
                array("email"    => "prueba1@prueba.com", 
                      "password" => "prueba",
                      "idFacebook" => "abc",
                      "dni" => "abc",
                      "name" => "abc",
                      "surname" => "abc",
                      "phone" => "abc",
                      "artisticName" => "abc",
                      "profilePhoto" => "abc",
                      "rol" => "abc"
                    ),      
                array("Content-Type" => "application/json"),
            ),
        );
    }

    /**
     * @dataProvider userProvider
     */
    public function testCreate($user, $headers) {
        $client = $this->createClient();
        $client->request("POST", "/register", array(), array(), $headers, json_encode($user));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["email"], "prueba1@prueba.com");    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testCreate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/users/".$removeId, array(), array(), array("Content-Type" => "application/json"), json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }

}

?>
<?php

use Silex\WebTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

 class VotesControllerTest extends WebTestCase {

    use LoginTrait;

    public function createApplication()
    {
        global $app;
        return $app;
    }

    
    function setUp() {
        parent::setUp();
        $this->login();
    } 

    function tearDown() {
        parent::tearDown();
        $this->logout();
    }

    
    public function testCreate() {
        $client = $this->createClient();
        $headers = array("Content-Type" => "application/json");

        //creacion de la provincia
        $province = array("name" => "provincia");
        $client->request("POST", "/provinces", array(), array(), $headers, json_encode($province));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "provincia");    

        $GLOBALS["idProvince"] = $data["id"];

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvince"];
        $city["name"] = "rio4";

        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"], "rio4");    
        $GLOBALS["idCity"] = $data["id"];

        // armo el array para votes
        $vote = array("province"    => $GLOBALS["idProvince"], 
                      "city" => $GLOBALS["idCity"],
                    );

        //creacion de un voto
        $client->request("POST", "/votes", array(), array(), $headers, json_encode($vote));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["city"]["id"],$GLOBALS["idCity"]); 
        $GLOBALS["removeId"] = $data["id"];
    }

     /**
     * @depends testCreate
     */
    public function testUpdate() {
        $client = $this->createClient();
        $headers = array("Content-Type" => "application/json");

        //creacion de la ciudad
        $city["province"] = $GLOBALS["idProvince"];
        $city["name"] = "Carlos Paz";

        $client->request("POST", "/cities", array(), array(), $headers, json_encode($city));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["name"],"Carlos Paz");    
        $GLOBALS["idCity2"] = $data["id"];

        // armo el array para editar votes
        $vote = array( "city" => $GLOBALS["idCity2"] );

        $removeId = $GLOBALS["removeId"];
        $this->assertTrue($GLOBALS["removeId"] != '');  
        $this->assertTrue($GLOBALS["removeId"] != null);  
        $client->request("PUT", "/votes/".$removeId, array(), array(), $headers, json_encode($vote));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data["city"]["id"], $GLOBALS["idCity2"]);    
        $GLOBALS["removeId"] = $data["id"];
    }

    /**
    *  @depends testUpdate
    */
    public function testDestroy(){
        $client = $this->createClient();
        $headers = array("Content-Type" => "application/json");
        $removeId = $GLOBALS["removeId"];
        $client->request("DELETE", "/votes/".$removeId, array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $removeId = $GLOBALS["idCity"];
        $client->request("DELETE", "/cities/".$removeId, array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $removeId = $GLOBALS["idCity2"];
        $client->request("DELETE", "/cities/".$removeId, array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $removeId = $GLOBALS["idProvince"];
        $client->request("DELETE", "/provinces/".$removeId, array(), array(), $headers, json_encode(array()));
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }

}

?>